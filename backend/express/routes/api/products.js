/* eslint-disable */
const router = require('express').Router();
const fetch = require('node-fetch');
/* eslint-enable */

getImg = (item) => {
	return new Promise((resolve) => {
		fetch(URL_BACKEND+'/V1/products/'+item.sku+'/media', {
			method: 'GET',
			headers: {
				'Authorization': 'Bearer '+ADMIN_TOKEN,
			},
		}).then((res) => res.json())
			.then((image) => {
				item.media_gallery_entries = image;
				resolve(item);
			});
	});
};

getProduct = (sku) => {
	return new Promise((resolve) => {
		fetch(URL_BACKEND+'/V1/products/'+sku, {
			method: 'GET',
			headers: {
				'Authorization': 'Bearer '+ADMIN_TOKEN,
			},
		}).then((res) => res.json())
			.then((product) => {
				resolve(product);
			});
	});
};

router.post('/getone', async (req, res) => {
	const asyncP = req.body.products.map((sku) => {
		return getProduct(sku);
	});
	Promise.all(asyncP).then((resp) => {
		res.status(200).json({product: resp});
	});
});

router.get('/categories', async (req, res) => {
	fetch(URL_BACKEND+'/V1/categories', {
		method: 'GET',
		headers: {
			'Authorization': 'Bearer '+ADMIN_TOKEN,
		},
	}).then((res) => res.json())
		.then((categories) => {
			res.status(200).json(categories);
		})
		.catch((error) => {
			res.status(400).json({error: error});
		});
});

router.get('/streamproduct', async (req, res) => {
	fetch(URL_BACKEND+ '/V1/products'+
		'?searchCriteria[filterGroups][0][filters][0][field]=streaming_product'+
		'& searchCriteria[filterGroups][0][filters][0][value]=1'+
		'& searchCriteria[filterGroups][0][filters][0][conditionType]=eq', {
		method: 'GET',
		headers: {
			'Authorization': 'Bearer '+ADMIN_TOKEN,
		},
	}).then((res) => res.json())
		.then(async (product) => {
			res.status(200).json(await getImg(product.items[0]));
		})
		.catch((error) => {
			res.status(400).json({error: error});
		});
});

module.exports = router;
