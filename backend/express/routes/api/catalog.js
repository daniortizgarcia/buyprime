/* eslint-disable */
const router = require('express').Router();
const fetch = require('node-fetch');
/* eslint-enable */

getCategory = (id) => {
	return new Promise((resolve) => {
		fetch(URL_BACKEND+'/V1/categories/'+id, {
			method: 'GET',
			headers: {
				'Authorization': 'Bearer '+ADMIN_TOKEN,
			},
		}).then((res) => res.json())
			.then((categories) => {
				resolve(categories);
			});
	});
};

getCategories = (id) => {
	return new Promise((resolve) => {
		fetch(URL_BACKEND+'/V1/products?'+
			'searchCriteria[filterGroups][0][filters][0][field]=category_id'+
			'& searchCriteria[filterGroups][0][filters][0][value]='+id+
			'& searchCriteria[filterGroups][0][filters][0][conditionType]=eq'+
			'&searchCriteria[sortOrders][0][field]=created_at'+
			'& searchCriteria[sortOrders][0][direction]=DESC'+
			'& searchCriteria[pageSize]=10', {
			method: 'GET',
			headers: {
				'Authorization': 'Bearer '+ADMIN_TOKEN,
			},
		}).then((res) => res.json())
			.then(async (categories) => {
				const cat = await getCategory(id);
				categories.name = cat.name;
				categories.level = cat.level;
				categories.children = cat.children;
				if (categories.children) {
					const asyncP = categories.children.split(',').map((id) => {
						return getCategories(id);
					});
					Promise.all(asyncP).then((resp) => {
						categories.childrenItems = resp;
						resolve(categories);
					});
				} else {
					resolve(categories);
				}
			})
			.catch((error) => {
				console.log(error);
			});
	});
};

router.post('/category', async (req, res) => {
	const category = await getCategory(req.body.id);
	if (category.children) {
		const asyncP = category.children.split(',').map((id) => {
			return getCategories(id);
		});
		Promise.all(asyncP).then((resp) => {
			res.status(200).json({category: category, categories: resp});
		});
	} else {
		res.status(200).json({
			category: category,
			categories: [await getCategories(category.id)],
		});
	}
});

module.exports = router;
