/* eslint-disable */
const router = require('express').Router();
const fetch = require('node-fetch');
/* eslint-enable */

router.get('/init', (req, res) => {
	fetch(URL_BACKEND+'/V1/integration/admin/token', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			'username': process.env.USER_ADMIN,
			'password': process.env.PASSWORD_ADMIN,
		}),
	}).then((res) => res.json())
		.then((token) => {
			global.ADMIN_TOKEN = token;
			res.status(200).json(true);
		});
});

module.exports = router;
