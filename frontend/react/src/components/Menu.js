import React from 'react';
import {Link} from 'react-router-dom';

/**
 * @class AMenu
 * @desc Global menu of the application
 * @param {Object} props
 * @return {JSX}
 */
const AMenu = (props) => {
	/**
	 * RegisteredMenu
	 * @desc When the user is registered in the app
	 * @param {Object} props
	 * @return {JSX}
	 */
	const RegisteredMenu = (props) => {
		return (
			<li className="nav-item ml-2 mr-2 li-logout"
				onClick={() => props.logout()}>
				<i className="fas fa-sign-out-alt icon"></i>
			</li>
		);
	};

	/**
	 * UnRegisteredMenu
	 * @desc When the user isn't registered in the app
	 * @return {JSX}
	 */
	const UnregisteredMenu = () => {
		return (
			<li className="nav-item ml-2 mr-2">
				<Link to="/login">
					<i className="fas fa-user icon text-dark"></i>
				</Link>
			</li>
		);
	};

	return (
		<ul className="navbar-nav flex-row align-items-center">
			<li className="nav-item mr-2 ml-2">
				<Link to="/video">
					{
						props.streamProduct ?
							<label className="stream-circle m-0"></label> : ''
					}
					<i className="fas fa-video text-dark icon"></i>
				</Link>
			</li>
			{
				props.user ?
					<RegisteredMenu user={props.user} logout={props.logout} />
					: <UnregisteredMenu />
			}
			<li className="nav-item ml-2 mr-2">
				<Link to="/cart" className="text-dark">
					<label className="number-items m-0">
						{props.nproducts}</label>
					<i className="fas fa-shopping-bag icon"></i>
				</Link>
			</li>
		</ul>
	);
};

export default AMenu;
