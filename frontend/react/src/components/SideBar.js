import React from 'react';
import CategoriesMenu from './MenuCategories';

/**
 * @class SideBar
 * @param {Object} props
 * @return {JSX}
 */
const SideBar = (props) => {
	const classVar = props.stateSidebar ? 'normalSideBar' : 'smallSideBar';
	return (
		<article className={'border sidebar ' + classVar} >
			{
				props.stateSidebar ? <section
					onClick={() => props.showSidebar(false)}
					className="p-1 text-center border-bottom changeSideBar c-pointer">
					<span><i className="fas fa-caret-left"></i></span>
				</section>
					: <section
						onClick={() => props.showSidebar(true)}
						className="p-1 text-center border-bottom changeSideBar c-pointer">
						<span><i className="fas fa-caret-right"></i></span>
					</section>
			}
			{
				props.stateSidebar ?
					<section>
						<CategoriesMenu
							categories={props.categories}
							changeLevel={props.changeLevel}/>
						<p className="p-2 text-center border-bottom small popularChannels">
							Populars Channels</p>
					</section>
					: ''
			}
			<section className="m-2">
				<section className="mt-1 mb-1 d-flex">
					<img className="sidebarImg"
						src="https://pbs.twimg.com/profile_images/895949432054796288/bEwXlvhK_400x400.jpg"
						alt="Logo Peluchitos"/>
					{
						props.stateSidebar ?
							<section className="ml-2 descriptionSidebar">
								<p className="mb-1">Devopensource</p>
								<p className="m-0 small">Ecommerce</p>
							</section>
							: ''
					}
				</section>
			</section>
		</article>
	);
};

export default SideBar;
