import React from 'react';

/**
 * @class 404
 * @return {JSX}
 */
const NotFound = () => {
	return (
		<section className="w-100 d-flex justify-content-center">
			<label className="w-50 not-found" ></label>
		</section>
	);
};

export default NotFound;
