import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import ProductListCart from './ProductListCart';

/**
 * @desc Import constants
 * @ignore
 */
import {
	REMOVE_ITEM_CART,
	CHANGE_QTY_CART,
} from '../../constants/actionTypes';
import agent from '../../agent';

/**
 * mapStateToProps
 * @param {Object} state
 * @return {*}
 * @ignore
 */
const mapStateToProps = (state) => {
	return {
		...state.cart,
	};
};

/**
 * mapDisptachToProps
 * @param {Object} dispatch
 * @return {*}
 * @ignore
 */
const mapDisptachToProps = (dispatch) => ({
	/**
	 * removeItem
	 * @desc Remove a item of the cart.
	 * @param {Integer} idItem
	 * @param {Integer} idCart
	 * @return {*}
	 * @ignore
	 */
	removeItem: (idItem, idCart) =>
		dispatch({type: REMOVE_ITEM_CART,
			payload: agent.Cart.removeItem(idItem, idCart), idItem}),

	/**
	 * changeQty
	 * @desc Change the quantity of item of the cart.
	 * @param {Object} item
	 * @return {*}
	 * @ignore
	 */
	changeQty: (item) =>
		dispatch({type: CHANGE_QTY_CART,
			payload: agent.Cart.updateItem(item)}),
});

/**
 * @classdesc Class Cart
 */
class Cart extends React.Component {
	/**
	 * @constructor
	 */
	constructor() {
		super();
		/**
		 * @desc Remove item of the cart
		 * @param {Integer} idItem
		 */
		this.removeItem = (idItem) => {
			this.props.removeItem(idItem, this.props.idCart);
		};
		/**
		 * @desc Change qty of item in the cart
		 * @param {Object} item
		 * @param {Integer} qty
		 */
		this.changeQty = (item, qty) => {
			item.qty = qty;
			this.props.changeQty(item);
		};
	}
	/**
	 * render
	 * @return {JSX}
	 */
	render() {
		let orderTotal = 0;
		if (this.props.cartItems.length > 1) {
			this.props.cartItems.map((item) => {
				orderTotal = orderTotal + (item.qty * item.price);
				return true;
			});
		} else if (this.props.cartItems.length > 0) {
			orderTotal = this.props.cartItems[0].qty * this.props.cartItems[0].price;
		}
		return (
			<article
				className="pt-5 pl-5 pr-5 w-100 overflow-y-scroll container-cart">
				<h3>Shopping Cart</h3>
				<article
					className="d-flex justify-content-between mt-4 container-list-cart">
					<ProductListCart
						products={this.props.cartItems}
						removeItem={this.removeItem}
						changeQty={this.changeQty} />
					<section className={`d-flex flex-column align-items-center border
						border-secondary rounded p-4 mb-3 fit-content summary-cart w-50`}>
						<section>
							<h3>Cart Summary</h3>
						</section>
						<hr className="border-dark w-100" />
						<section>
							<p><strong>Order Total</strong> {orderTotal} $</p>
						</section>
						<Link to="/personaldata" className="nav-brand">
							<button className="btn btn-primary"
								disabled={!orderTotal}>
							Proceed to Checkout</button>
						</Link>
					</section>
				</article>
			</article>
		);
	}
}

export default connect(mapStateToProps, mapDisptachToProps)(Cart);
