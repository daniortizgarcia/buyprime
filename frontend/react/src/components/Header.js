import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import AMenu from './Menu';

/**
 * mapStateToProps
 * @param {Object} state
 * @return {Object}
 * @ignore
 */
const mapStateToProps = (state) => {
	return {
		...state.common,
		cartItems: state.cart.cartItems,
	};
};

/**
 * @classdesc Class Hedaer
 */
class Header extends React.Component {
	/**
	 * render
	 * @return {JSX} JSX del Header
	 */
	render() {
		let nproducts = 0;
		if (this.props.cartItems.length > 1) {
			this.props.cartItems.map((item) => {
				nproducts = nproducts + item.qty;
				return true;
			});
		} else if (this.props.cartItems.length > 0) {
			nproducts = this.props.cartItems[0].qty;
		} else {
			nproducts = 0;
		}

		return (
			<nav className={`d-flex flex-column shadow-sm
				pt-2 pb-2 pl-3 pr-3 position-relative`}>
				<section className="d-flex justify-content-between pl-4 pr-4">
					<section className="text-center">
						<Link to="/">
							<img src="/logo.svg" alt="Page logo" width="27px" />
						</Link>
					</section>
					<AMenu user={this.props.userInfo}
						streamProduct={this.props.streamProduct}
						logout={this.props.logout}
						nproducts={nproducts} />
				</section>
			</nav>
		);
	}
}

export default connect(mapStateToProps)(Header);
