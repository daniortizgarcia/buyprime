import React from 'react';
import {connect} from 'react-redux';
import ProductList from '../ProductList';

import {
	LIST_CATALOG,
	ADD_ITEM_CART,
	UNMOUNT_CATALOG,
	CHANGE_BUTTON_STATE,
} from '../../constants/actionTypes';
import agent from '../../agent';

const mapStateToProps = (state) => {
	return {
		...state.catalog,
		buttAddDisabled: state.cart.buttAddDisabled,
		idCart: state.cart.idCart,
	};
};

const mapDispatchToProps = (dispatch, props) => ({
	/**
	 * listCatalog
	 * @return {*}
	 * @ignore
	 */
	listCatalog: () =>
		dispatch({type: LIST_CATALOG,
			payload: agent.Catalog.category(props.match.params.id),
			props}),

	/**
	 * addCart
	 * @desc Save product in the cart
	 * @param {String} product
	 * @param {Integer} idCart
	 * @return {*}
	 * @ignore
	 */
	addCart: (product, idCart) =>
		dispatch({type: ADD_ITEM_CART,
			payload: agent.Cart.addItem(product.sku, idCart), product}),

	/**
	 * changeButtState
	 * @return {*}
	 * @ignore
	 */
	changeButtState: () =>
		dispatch({type: CHANGE_BUTTON_STATE}),

	/**
	 * unmount
	 * @return {*}
	 * @ignore
	 */
	unmount: () =>
		dispatch({type: UNMOUNT_CATALOG}),
});

/**
 * @classdesc Class Catalog
 */
class Catalog extends React.Component {
	/**
	 * @constructor
	 */
	constructor() {
		super();
		this.state = {
			buttAddDisabled: false,
		};
		/**
		 * @desc Add item to the cart
		 * @param {Object} product
		 */
		this.addCart = (product) => {
			this.setState({
				buttAddDisabled: true,
			});
			this.props.addCart(product, this.props.idCart);
		};
		/**
		 * @desc Change state of the button add to cart
		 */
		this.changeButtState = () => {
			this.setState({
				buttAddDisabled: false,
			});
			this.props.changeButtState();
		};
	}
	/**
	 * componentDidMount
	 */
	componentDidMount() {
		this.props.listCatalog();
	}
	/**
	 * componentWillUnmount
	 */
	componentWillUnmount() {
		if (this.props.category) {
			this.props.unmount();
		}
	}
	/**
	 * componentDidUpdate
	 */
	componentDidUpdate() {
		if (this.props.buttAddDisabled && this.state.buttAddDisabled) {
			this.changeButtState();
		}
	}
	/**
	 * render
	 * @return {JSX}
	 */
	render() {
		return (
			this.props.category && this.props.categories.length > 0 ?
				<section className={`flex-column overflow-y-scroll
					align-items-center wfit-content pt-3`}>
					<h2 className="text-center">{ this.props.category.level === 2 ?
						this.props.category.name : ''}</h2>
					{
						this.props.categories.map((category, i) => {
							return <section className={`d-flex flex-column
								align-items-center mt-3 mb-3`} key={i}>
								<h3>{category.name}</h3>
								{
									category.items.length > 0 || !category.children ?
										<section className="d-flex flex-wrap catalog-products">
											<ProductList
												products={category.items}
												addCart={this.addCart}
												buttAddDisabled={this.state.buttAddDisabled}/>
										</section> :

										category.childrenItems.map((category, i) => {
											return <section className={`d-flex flex-column
												align-items-center mt-3 mb-3`} key={i}>
												<h3>{category.name}</h3>
												<section className="d-flex flex-wrap catalog-products">
													<ProductList
														products={category.items}
														addCart={this.addCart}
														buttAddDisabled={this.state.buttAddDisabled}/>
												</section>
											</section>;
										})
								}
							</section>;
						})
					}
				</section>
				: <label className={`mt-5 mb-5 text-center loader-spinner`}></label>
		);
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(Catalog);
