import React from 'react';

import MessageList from './MessageList';

/**
 * @class Messages
 * @param {Object} props
 * @return {JSX}
 */
const Messages = (props) => {
	return (
		<section className="h-100 messages d-flex flex-column">
			{
				props.channel ?
					<section className="d-flex align-items-center p-2 border-bottom">
						<img className="m-3 pr-0 chatImage col-auto" src={props.cover}
							alt="logo channel"/>
						<p className="m-0 p-3">{props.channel}</p>
					</section>
					: <p className="p-3 border-bottom col-auto">Load Channel...</p>
			}
			<section className="messagesHeight overflow-auto col" id="messChat"> {
				props.messages ?
					props.messages.map((message, i) => {
						return	<section key={i} className="m-3 mb-2 small">
							<MessageList message={message} />
						</section>;
					}) :
					<section
						className="h-100 d-flex justify-content-center align-items-center">
						<label className="lds-dual-ring"></label>
					</section>
			}</section>

			<form onSubmit={props.saveMessage}
				className="m-3 justify-content-end nresize col-auto">
				<fieldset>
					<textarea
						required
						type="text"
						placeholder="Add Message"
						value={props.message || ''}
						onChange={props.changeInput}
						className="p-2 border rounded small align-top w-100 nresize"
						disabled={!props.nickname}>
					</textarea>
				</fieldset>
				<button
					className="mt-2 btn btn-primary float-right"
					type="submit"
					disabled={props.buttDisabled || !props.nickname}>
					{
						props.nickname ?
							'Chat'
							: 'Register'
					}
				</button>
			</form>
		</section>
	);
};

export default Messages;
