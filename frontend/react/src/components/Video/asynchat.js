/**
 * @constant asynchat
 * @ignore
 */
export const asynchat = {
	/**
	 * @function connect
	 * @desc Connect to SendBird server and edit the user
	 * @param {String} nickname
	 * @param {String} email
	 * @param {Object} sb
	 * @return {Promise}
	 */
	connect: (nickname, email, sb) => {
		return new Promise((resolve) => {
			sb.connect(email,
				(user, error) => {
					if (error) {
						resolve(error.toString());
					}

					resolve(user ? !user.nickname ?
						sb.updateCurrentUserInfo(nickname, '', (response, error) => {})
						: true : true);
				});
		});
	},
	/**
	 * @function searchchannel
	 * @desc Search a channel for the product
	 * @param {Object} channelList
	 * @return {Promise}
	 */
	searchchannel: (channelList) => {
		return new Promise((resolve) => {
			channelList.next((channels, error) => {
				resolve(channels ? channels[0] ? channels[0].url: false : false);
			});
		});
	},
	/**
	 * @function createchannel
	 * @desc Create a channel for the product
	 * @param {String} name
	 * @param {String} image
	 * @param {Object} sb
	 * @return {Promise}
	 */
	createchannel: (name, image, sb) => {
		return new Promise((resolve) => {
			sb.OpenChannel.createChannel(name, image, null, (channel, error) => {
				resolve(channel ? channel.url : false);
			});
		});
	},
	/**
	 * @function getChannel
	 * @desc Get channel and enter
	 * @param {Object} sb
	 * @param {Function} initChat
	 * @param {String} url
	 * @param {String} sku
	 * @return {Promise}
	 */
	getchannel: (sb, initChat, url, sku) => {
		return new Promise((resolve) => {
			sb.OpenChannel.getChannel(url, (channel, error) => {
				if (channel) {
					const messageListQuery = channel.createPreviousMessageListQuery();
					messageListQuery.limit = 30;
					messageListQuery.reverse = true;

					channel.enter(function(response, error) {
						messageListQuery.load(function(messageList, error) {
							initChat(channel.name, channel.coverUrl, messageList, sku);
						});
						resolve(channel);
					});
				}
			});
		});
	},
	/**
	 * @function messagereceived
	 * @desc When the chat detect a new message save in the store
	 * @param {Object} channel
	 * @param {Function} saveMessage
	 * @param {Object} that
	 * @return {Promise}
	 */
	messagereceived: (channel, saveMessage, that) => {
		return new Promise((resolve) => {
			channel.onMessageReceived = (channel, message) => {
				saveMessage(message);
				that.setState({reaload: true});
			};

			resolve(true);
		});
	},
	/**
	 * @function exit
	 * @desc Exit of the channel of the SendBird
	 * @param {Object} channel
	 * @return {Promise}
	 */
	exit: (channel) => {
		return new Promise((resolve) => {
			channel.exit((response, error) => {
				if (error) {
					resolve(false);
				}
			});

			resolve(true);
		});
	},
};
