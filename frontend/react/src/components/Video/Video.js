import React from 'react';
import {connect} from 'react-redux';

import ReactPlayer from 'react-player';
import SendBird from 'sendbird';

import Messages from './Messages';

/**
 * @desc Import constants
 * @ignore
 */
import {
	INIT_CHAT,
	CHANGE_INPUT_CHAT,
	SAVE_MESSAGE,
	LIST_PRODUCT_STREAM,
	ADD_ITEM_CART,
	UNMOUNT_VIDEO,
	CHANGE_BUTTON_STATE,
	SELECT_VIDEO_IMG,
	SET_PRODUCT_STREAM,
} from '../../constants/actionTypes';
import agent from '../../agent';
import {asynchat} from './asynchat';
import {utils} from '../../utils';

const sb = new SendBird({appId: process.env.REACT_APP_SENDBIRD_ID});
/**
 * mapStateToProps
 * @param {*} state
 * @return {Object}
 * @ignore
 */
const mapStateToProps = (state) => {
	return {
		...state.video,
		...state.common,
		idCart: state.cart.idCart,
		buttAddDisabled: state.cart.buttAddDisabled,
		nickname: state.common.userInfo ?
			state.common.userInfo.firstname+''+state.common.userInfo.lastname : null,
		usermail: state.common.userInfo ? state.common.userInfo.email : null,
	};
};

/**
 * mapDispatchToProps
 * @param {Object} dispatch
 * @param {Object} props
 * @return {*}
 * @ignore
 */
const mapDispatchToProps = (dispatch, props) => ({
	/**
	 * getInfoChat
	 * @param {String} channel
	 * @param {String} cover
	 * @param {Array} messages
	 * @param {String} sku
	 * @desc Set channel
	 * @return {*}
	 * @ignore
	 */
	initChat: (channel, cover, messages, sku) =>
		dispatch({type: INIT_CHAT, channel, cover, messages, sku, props}),

	/**
	 * getSubCat
	 * @return {*}
	 * @ignore
	 */
	getProduct: () =>
		dispatch({type: LIST_PRODUCT_STREAM, props,
			payload:
				agent.Products.getProduct([props.match.params.sku])}),

	/**
	 * setProduct
	 * @param {Object} product
	 * @return {*}
	 * @ignore
	 */
	setProduct: (product) =>
		dispatch({type: SET_PRODUCT_STREAM, product}),

	/**
	 * changeInput
	 * @param {String} message
	 * @desc Change value of the input
	 * @return {*}
	 * @ignore
	 */
	changeInput: (message) =>
		dispatch({type: CHANGE_INPUT_CHAT, message}),

	/**
	 * saveMessage
	 * @param {String} message
	 * @desc Add a new message
	 * @return {*}
	 * @ignore
	 */
	saveMessage: (message) =>
		dispatch({type: SAVE_MESSAGE, message}),

	/**
	 * addCart
	 * @desc Save product in the cart
	 * @param {String} product
	 * @param {Integer} idCart
	 * @return {*}
	 * @ignore
	 */
	addCart: (product, idCart) =>
		dispatch({type: ADD_ITEM_CART,
			payload: agent.Cart.addItem(product.sku, idCart), product}),

	/**
	 * changeButtState
	 * @return {*}
	 * @ignore
	 */
	changeButtState: () =>
		dispatch({type: CHANGE_BUTTON_STATE}),

	/**
	 * selectImg
	 * @param {String} img
	 * @ignore
	 */
	selectImg: (img) => {
		utils.checkIfExist(
			document.getElementById('scrollTopVideo'),
			0,
			'scrollTop');
		dispatch({type: SELECT_VIDEO_IMG, img});
	},

	/**
	 * unmount
	 * @desc Clear data video
	 * @return {*}
	 * @ignore
	 */
	unmount: () =>
		dispatch({type: UNMOUNT_VIDEO}),
});

/**
 * @classdesc Video
 */
class Video extends React.Component {
	/**
	 * @desc Constructor
	 */
	constructor() {
		super();
		this.state = {
			buttAddDisabled: false,
		};
		const that = this;
		this.openChannel = '';
		/**
		 * @desc Change value of the Input
		 * @param {Object} ev
		 */
		this.changeInput = (ev) => {
			this.props.changeInput(ev.target.value);
		};
		/**
		 * @desc Add item to the cart
		 * @param {Object} product
		 */
		this.addCart = (product) => {
			this.setState({
				buttAddDisabled: true,
			});
			this.props.addCart(product, this.props.idCart);
		};
		/**
		 * @desc Change the state of the button
		 */
		this.changeButtState = () => {
			this.setState({
				buttAddDisabled: false,
			});
			this.props.changeButtState();
		};
		/**
		 * @desc Save the new message
		 * @param {Object} ev
		 * @return {*}
		 */
		this.saveMessage = (ev) => {
			ev.preventDefault();
			return this.openChannel ?
				this.openChannel.sendUserMessage(this.props.message,
					(message, error) => {
						return !error ? that.props.saveMessage(message) : false;
					})
				: false;
		};
		/**
		 * @desc Get the url of the product
		 * @return {String}
		 */
		this.getVideo = () => {
			let linkV;
			if (this.props.product) {
				this.props.product.custom_attributes.map((attr) => {
					if (attr.attribute_code === 'video_link') {
						linkV = attr.value;
					}
					return true;
				});
			}
			return linkV;
		};
	}

	/**
	 * componentDidMount
	 * @desc Listener from SendBird to Save Messages, Connect in the chat.
	 */
	async componentDidMount() {
		const ChannelHandler = new sb.ChannelHandler();
		const openChannelListQuery = sb.OpenChannel.createOpenChannelListQuery();
		const that = this;
		let url;

		if (this.props.nickname) {
			await asynchat.connect(
				this.props.nickname, this.props.usermail.split('@')[0], sb);
		} else {
			await asynchat.connect('anonymous', 'anonymous', sb);
		}

		if (this.props.product) {
			openChannelListQuery.nameKeyword = this.props.product.name;
			url = await asynchat.searchchannel(openChannelListQuery);
			if (!url) {
				url = await asynchat.createchannel(this.props.product.name,
					process.env.REACT_APP_MAGENTO_MEDIA_URL +
					this.props.product.media_gallery_entries[0].file, sb);
			}

			await asynchat.messagereceived(ChannelHandler,
				that.props.saveMessage,
				that);
			sb.addChannelHandler('208549964', ChannelHandler);
			this.openChannel = await asynchat.getchannel(sb, that.props.initChat, url,
				that.props.product.sku);
		}
	}

	/* eslint-disable*/
	/**
	 * componentWillMount
	 * @desc Get the products.
	 */
	componentWillMount() {
		if (this.props.channelName) {
            window.location.reload();
		}
		if (Object.keys(this.props.match.params).length === 0) {
			if (!this.props.streamProduct) {
				this.props.history.push('/');
			} else {
				this.props.setProduct(this.props.streamProduct);
			}
		} else {
			this.props.getProduct();
		}
	}
	/* eslint-enable*/

	/**
	 * componentWillUnmount
	 * @desc Disconnect user from Channel and SendBird. Clear data.
	 */
	async componentWillUnmount() {
		this.openChannel ?
			await asynchat.exit(this.openChannel)
			: await ({});
		if (this.props.product) {
			this.props.unmount();
		}
		sb.disconnect(() => {
		});
	}

	/**
	 * componentDidUpdate
	 * @desc Enable the button when add the item.
	 */
	componentDidUpdate() {
		if (this.props.buttAddDisabled && this.state.buttAddDisabled) {
			this.changeButtState();
		}

		utils.checkIfExist(
			document.getElementById('messChat'),
			100000,
			'scrollTop');
	};

	/**
	 * render
	 * @return {JSX} JSX del video
	 */
	render() {
		const urlVideo = this.getVideo();
		return (
			this.props.product ?
				<article className="d-flex h-100 fullHeight w-100 overflow-y-scroll">
					<section className="content-video d-flex w-100">
						<section
							className={`video-displayer m-3 border rounded-bottom
								fit-content overflow-y-scroll`} id="scrollTopVideo">
							{ !this.props.imgSelected ?
								<ReactPlayer url={ urlVideo || 'http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4'}
									playing
									controls={true} />
								: <section className="d-flex flex-column align-items-end">
									<i className={`far fa-times-circle text-secondary 
										h4 m-0 c-pointer fit-content mt-2 mr-4`}
									onClick={() => this.props.selectImg(false)}></i>
									<section className="d-flex justify-content-center w-100">
										<img src={process.env.REACT_APP_MAGENTO_MEDIA_URL+
											this.props.imgSelected} width="46%"
										className="h-100"
										alt="Detiails thumblain product"/>
									</section>
								</section>
							}
							<section className="d-flex flex-wrap justify-content-around mt-2">
								<section
									className={`w-100 d-flex m-3 flex-wrap
										justify-content-around align-items-center`}>
									<h5 className="mb-0">{this.props.product.name}</h5>
									<button
										className="btn btn-primary"
										type="button"
										disabled={this.state.buttAddDisabled}
										onClick={() =>
											this.addCart({
												id: this.props.product.id,
												name: this.props.product.name,
												sku: this.props.product.sku,
												price: this.props.product.price,
												image: this.props.product.media_gallery_entries[0].file,
												qty: 1})}>
										{this.props.product.price} $
									</button>
								</section>
								{	this.props.product.media_gallery_entries ?
									this.props.product.media_gallery_entries.map((img, i) =>{
										return <img key={i}
											className="border m-2 w-25 h-100 c-pointer"
											src={process.env.REACT_APP_MAGENTO_MEDIA_URL+img.file}
											alt="preview product"
											onClick={() => this.props.selectImg(img.file)} />;
									}) : ''
								}
								{
									this.props.product.custom_attributes.map((att, i) => {
										return att.attribute_code === 'description' ?
											<section className="m-3" key={i}>
												<h4 className="mb-2">Description</h4>
												<label dangerouslySetInnerHTML={
													{__html: att.value}}
												className="small text-justify"></label>
											</section> : '';
									})
								}
							</section>
						</section>
						<section className="video-chat border">
							<Messages
								channel={this.props.channelName}
								cover={this.props.channelCover}
								messages={this.props.messages}
								saveMessage={this.saveMessage}
								changeInput={this.changeInput}
								message={this.props.message}
								buttDisabled={this.props.buttDisabled}
								nickname={this.props.nickname} />
						</section>
					</section>
				</article>
				: <label className={`mt-5 mb-5 text-center loader-spinner`}></label>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Video);
