import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import agent from '../../agent';
import {Input} from '../Form/Input';

import Errors from '../Errors';
/**
 * @desc Import constants
 * @ignore
 */
import {
	CHANGE_INPUT_AUTH,
	CHANGE_TYPE_PASS,
	SHOW_ERRORS_REGISTER,
	REGISTER,
	UNMOUNT_REGISTER,
} from '../../constants/actionTypes';
import {validate} from './validate';
import {InputPass} from '../Form/InputPass';

/**
 * mapStateToProps
 * @param {Object} state
 * @return {*}
 * @ignore
 */
const mapStateToProps = (state) => ({
	...state.auth,
	token: state.common.token,
});

const mapDispatchToProps = (dispatch, props) => ({
	/**
	 * changeInput
	 * @desc Save in store the changes of the input
	 * @param {String} key
	 * @param {String} value
	 * @return {*}
	 * @ignore
	 */
	changeInput: (key, value) =>
		dispatch({type: CHANGE_INPUT_AUTH, key, value}),

	/**
	 * changePassType
	 * @desc Change type of the input password in the store
	 * @param {String} key
	 * @param {String} passType
	 * @return {*}
	 * @ignore
	 */
	changePassType: (key, passType) =>
		dispatch({type: CHANGE_TYPE_PASS, key, passType}),

	/**
	 * showErrors
	 * @desc Save the errors to show in Login component
	 * @param {Array} errors
	 * @return {*}
	 * @ignore
	 */
	showErrors: (errors) =>
		dispatch({type: SHOW_ERRORS_REGISTER, errors}),

	/**
	 * register
	 * @description Register the user in the application.
	 * @param {String} user
	 * @param {String} password
	 * @ignore
	 */
	register: (user) => {
		dispatch({type: REGISTER, payload: agent.Auth.createCustomer(user), props});
	},

	/**
	 * unmount
	 * @desc unmount the component
	 * @return {*}
	 * @ignore
	 */
	unmount: () =>
		dispatch({type: UNMOUNT_REGISTER}),
});

/**
 * @classdesc Class Register
 */
class Register extends React.Component {
	/**
	 * @constructor
	 */
	constructor() {
		super();
		/**
		 * @desc Initial state Register component. disable button from the form,
		 * unmount to know when can unmount the component.
		 */
		this.state = {
			buttRdisable: false,
			unmount: true,
		};
		/**
		 * @desc Change value of the Input
		 * @param {String} key
		 * @return {Object}
		 */
		this.changeInput = (key) => (ev) => {
			this.props.changeInput(key, ev.target.value);
		};
		/**
		 * @desc Register user in the app
		 * @param {Object} ev
		 */
		this.submitForm = async (ev) => {
			ev.preventDefault();
			this.setState({
				buttRdisable: true,
				unmount: false,
			});
			/**
			 * @desc Call function validateFormReg to validate the field of the form
			 */
			const stateForm =
				await validate.validateFormReg(
					this.props.fname,
					this.props.lname,
					this.props.emailR,
					this.props.passwordR,
					this.props.cpasswordR);
			/**
			 * @desc If stateForm have errors, show errors else call
			 * the nodejs backend
			 */
			if (stateForm.length > 0) {
				this.setState({
					buttRdisable: false,
					unmount: true,
				});
				this.props.showErrors(stateForm);
				document.getElementById('scrollTopRegister').scrollTop = 0;
			} else {
				const magJson = {
					'customer': {
						'email': this.props.emailR,
						'firstname': this.props.fname,
						'lastname': this.props.lname,
					},
					'password': this.props.passwordR,
				};
				this.props.register(magJson);
			}
		};
	}
	/**
	 * componentDidMount
	 * @desc Check if user is loged in the app.
	 */
	componentDidMount() {
		if (this.props.token) {
			this.props.history.push('/');
		}
	}
	/**
	 * getDerivedStateFromProps
	 * @desc Validate to know when disable the button
	 * @param {*} props
	 * @param {*} state
	 * @return {Boolean}
	 */
	static getDerivedStateFromProps(props, state) {
		if (props.errorsRegister) {
			if (props.errorsRegister.length > 0 &&
				props.errorsRegister !== state.errorsRegister) {
				return {
					buttRdisable: false,
					unmount: true,
					errorsRegister: props.errorsRegister,
				};
			}
		}
		return true;
	}
	/**
	 * componentWillUnmount
	 */
	componentWillUnmount() {
		if (this.state.unmount) {
			this.props.unmount();
		}
	}
	/**
	 * render
	 * @return {JSX}
	 */
	render() {
		return (
			<section
				className={`w-100 overflow-y-scroll d-flex flex-column
					align-items-center pt-4 pb-3 container-auth`}
				id="scrollTopRegister">
				<Errors errors={this.props.errorsRegister} />
				<section className="w-50 authForm border mt-4 rounded bg-white">
					<form className="p-5 w-100 auhtForm" onSubmit={this.submitForm}>
						<fieldset className="form-row d-flex flex-column">
							<p className="m-0 text-dark h3">Personal Information</p>
							<hr className="mt-2 mb-2" />
							<Input type='text' name='firstname' labelName='First Name'
								placeholder='First Name' storeName='fname'
								value={this.props.fname} changeInput={this.changeInput} />
							<Input type='text' name='lastname' labelName='Last Name'
								placeholder='Last Name' storeName='lname'
								value={this.props.lname} changeInput={this.changeInput} />
						</fieldset>
						<fieldset className="form-row d-flex flex-column">
							<p className="m-0 text-dark h3">Sign-in Information</p>
							<hr className="mt-2 mb-2" />
							<Input type='email' name='email' labelName='Email'
								placeholder='Email' storeName='emailR'
								value={this.props.emailR} changeInput={this.changeInput} />

							<InputPass name='password' value={this.props.passwordR}
								labelName='Password' storeName='passwordR'
								changeInput={this.changeInput} passName='passRegType'
								placeholder='Password' passType={this.props.passRegType}
								changePassType={this.props.changePassType} />
							<InputPass name='cpassword' value={this.props.cpasswordR}
								labelName='Confirm Password' storeName='cpasswordR'
								changeInput={this.changeInput} passName='passRegCType'
								placeholder='Confirm Password'
								passType={this.props.passRegCType}
								changePassType={this.props.changePassType} />
						</fieldset>
						<p className="text-danger small">* Required Fields</p>
						<button
							type="submit"
							className="btn btn-primary w-100 mb-3"
							disabled={this.state.buttRdisable}>
							Create an Account</button>
						<Link to="/login">
							<label className="m-0 pr-2 text-dark">{
								`Have an account?`}</label>
							Log In
						</Link>
					</form>
				</section>
			</section>
		);
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
