import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import agent from '../../agent';

import Errors from '../Errors';
import {Input} from '../Form/Input';
import {InputPass} from '../Form/InputPass';

/**
 * @desc Import constants
 * @ignore
 */
import {
	CHANGE_INPUT_AUTH,
	CHANGE_TYPE_PASS,
	SHOW_ERRORS_LOGIN,
	LOGIN,
	UNMOUNT_LOGIN,
} from '../../constants/actionTypes';
import {validate} from './validate';

/**
 * @constant mapStateToProps
 * @param {Object} state
 * @return {*}
 * @ignore
 */
const mapStateToProps = (state) => ({
	...state.auth,
	token: state.common.token,
});

/**
 * mapDispatchToProps
 * @param {Object} dispatch
 * @param {Object} props
 * @return {*}
 * @ignore
 */
const mapDispatchToProps = (dispatch, props) => ({
	/**
	 * changeInput
	 * @desc Save in store the changes of the input
	 * @param {String} key
	 * @param {String} value
	 * @return {*}
	 * @ignore
	 */
	changeInput: (key, value) =>
		dispatch({type: CHANGE_INPUT_AUTH, key, value}),

	/**
	 * changePassType
	 * @desc Change type of the input password in the store
	 * @param {String} key
	 * @param {String} passType
	 * @return {*}
	 * @ignore
	 */
	changePassType: (key, passType) =>
		dispatch({type: CHANGE_TYPE_PASS, key, passType}),

	/**
	 * showErrors
	 * @desc Save the errors to show in Login component
	 * @param {Array} errors
	 * @return {*}
	 * @ignore
	 */
	showErrors: (errors) =>
		dispatch({type: SHOW_ERRORS_LOGIN, errors}),

	/**
	 * login
	 * @description Log in the user in the application.
	 * @param {String} user
	 * @param {String} password
	 * @ignore
	 */
	login: (user, password) => {
		dispatch({type: LOGIN,
			payload: agent.Auth.loginCustomer(user, password), props});
	},

	/**
	 * unmount
	 * @desc unmount the component
	 * @return {*}
	 * @ignore
	 */
	unmount: () =>
		dispatch({type: UNMOUNT_LOGIN}),
});

/**
 * @classdesc Class Login
 */
class Login extends React.Component {
	/**
	 * @constructor
	 */
	constructor() {
		super();
		/**
		 * Initial state Login component. disable button from the form,
		 * unmount to know when can unmount the component.
		 */
		this.state = {
			buttLdisable: false,
			unmount: true,
		};
		/**
		 * @desc Change value of the Input
		 * @param {String} key
		 * @return {Object}
		 */
		this.changeInput = (key) => (ev) => {
			this.props.changeInput(key, ev.target.value);
		};
		/**
		 * @desc Login in the app
		 * @param {Object} ev
		 */
		this.submitForm = async (ev) => {
			ev.preventDefault();
			this.setState({
				buttLdisable: true,
				unmount: false,
			});
			/**
			 * @desc Call function validateFormLog to validate the field of the form
			 */
			const stateForm =
				await validate.validateFormLog(this.props.emailL, this.props.passwordL);
			/**
			 * @desc If stateForm have errors, show errors else call
			 * the nodejs backend
			 */
			if (stateForm.length > 0) {
				this.setState({
					buttLdisable: false,
					unmount: true,
				});
				this.props.showErrors(stateForm);
			} else {
				this.props.login(this.props.emailL, this.props.passwordL);
			}
		};
	}
	/**
	 * componentDidMount
	 * @desc Check if user is loged in the app.
	 */
	componentDidMount() {
		if (this.props.token) {
			this.props.history.push('/');
		}
	}
	/**
	 * getDerivedStateFromProps
	 * @desc Validate to know when disable the button
	 * @param {*} props
	 * @param {*} state
	 * @return {Boolean}
	 */
	static getDerivedStateFromProps(props, state) {
		if (props.errorsLogin) {
			if (props.errorsLogin.length > 0 &&
				props.errorsLogin !== state.errorsLogin) {
				return {
					buttLdisable: false,
					unmount: true,
					errorsLogin: props.errorsLogin,
				};
			}
		}
		return true;
	}
	/**
	 * componentWillUnmount
	 */
	componentWillUnmount() {
		if (this.state.unmount) {
			this.props.unmount();
		}
	}
	/**
	 * render
	 * @return {JSX}
	 */
	render() {
		return (
			<section
				className={`w-100 overflow-y-scroll d-flex flex-column
					align-items-center pt-4 pb-3 container-auth justify-content-center`}>
				<Errors errors={this.props.errorsLogin} />
				<section className="w-50 authForm border rounded bg-white">
					<form className="p-5 w-100" onSubmit={this.submitForm}>
						<fieldset className="form-row d-flex flex-column">
							<p className="m-0 text-dark h3">Customer Login</p>
							<hr className="mt-2 mb-2" />
							<Input type='email' name='email' labelName='Email'
								placeholder='Your Email' storeName='emailL'
								value={this.props.emailL} changeInput={this.changeInput} />

							<InputPass name='password' value={this.props.passwordL}
								labelName='Password' storeName='passwordL'
								changeInput={this.changeInput} passName='passLogType'
								placeholder='Password' passType={this.props.passLogType}
								changePassType={this.props.changePassType} />
						</fieldset>
						<p className="text-danger small">* Required Fields</p>
						<button
							type="submit"
							className="btn btn-primary w-100 mb-3"
							disabled={this.state.buttLdisable}>
							Sign In</button>
						<Link to="/register">
							<label className="m-0 pr-2 text-dark">{
								`Don't have an account?`
							}</label>
							Register
						</Link>
					</form>
				</section>
			</section>
		);
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
