import React from 'react';
import {Link} from 'react-router-dom';

/**
 * @class CategoriesMenu
 * @param {Object} props
 * @return {JSX}
 */
const CategoriesMenu = (props) => {
	/**
	 * showMenu
	 * @desc Show categories menu
	 * @param {Object} categories
	 * @param {Function} changeLevel
	 * @return {JSX}
	 */
	const showMenu = (categories, changeLevel) => {
		return <section className="d-flex">
			{categories[0].level > 2 ?
				<i onClick={() => changeLevel(false)}
					className={`fas fa-chevron-left p-3 c-pointer border-bottom
						d-flex  align-items-center`}></i> : ''
			}
			<ul className="navbar-nav flex-column w-100">
				{categories.map((category, i) => {
					return <li className={`pl-2 nav-item dropd pt-2 pb-2
						border-bottom border-left d-flex`} key={i}>
						<Link to={'/category/'+
							category.name.toLowerCase().replace(/ /g, '-')+'/'+
							category.id}
						className="c-pointer">
							<label
								className="mb-0 pl-2 pr-2 mr-2 c-pointer text-dark small">
								{category.name}</label>
						</Link>
						{category.children_data.length > 0 ?
							<section>
								<i onClick={() => changeLevel(category.children_data)}
									className="fas fa-chevron-right p-2 c-pointer"></i>
							</section> : ''
						}
					</li>;
				})}
			</ul>
		</section>;
	};

	return (
		props.categories ?
			showMenu(props.categories, props.changeLevel)
			: ''
	);
};

export default CategoriesMenu;
