import React from 'react';
import {connect} from 'react-redux';

/**
 * mapStateToProps
 * @param {Object} state
 * @return {Object}
 * @ignore
 */
const mapStateToProps = (state) => {
	return {
		...state.common,
		idCart: state.cart.idCart,
	};
};

/**
 * @classdesc Class Home
 */
class Home extends React.Component {
	/**
	 * componentDidUpdate
	 * @desc Init a new cart if is login but, haven't a cart
	 * @param {*} prevProps
	 */
	componentDidMount() {
		if (!this.props.idCart && this.props.userInfo) {
			this.props.onLoad(this.props.userInfo.id);
		}
	}
	/**
	 * render
	 * @return {JSX} JSX del Home
	 */
	render() {
		return (
			<article className="text-center w-100 overflow-y-scroll">
				<div id="carousel" className="carousel slide" data-ride="carousel">
					<ol className="carousel-indicators">
						<li data-target="#carousel" data-slide-to="0"className="active">
						</li>
						<li data-target="#carousel" data-slide-to="1"></li>
						<li data-target="#carousel" data-slide-to="2"></li>
					</ol>
					<div className="carousel-inner">
						<div className="carousel-item active">
							<section className="d-block w-100 all-slides slide-1"></section>
						</div>
						<div className="carousel-item">
							<section className="d-block w-100 all-slides slide-2"></section>
						</div>
						<div className="carousel-item">
							<section className="d-block w-100 all-slides slide-3"></section>
						</div>
					</div>
					<a className="carousel-control-prev"
						href="#carousel" role="button" data-slide="prev">
						<span className="carousel-control-prev-icon"
							aria-hidden="true"></span>
						<span className="sr-only">Previous</span>
					</a>
					<a className="carousel-control-next"
						href="#carousel" role="button" data-slide="next">
						<span className="carousel-control-next-icon"
							aria-hidden="true"></span>
						<span className="sr-only">Next</span>
					</a>
				</div>
				<section className="container-image-home">
					<section className="image-home image-home-1" onClick={() =>
						this.props.history.push('/category/men/11')}>
						Men{`'`}s fashion</section>
					<section className="image-home image-home-2" onClick={() =>
						this.props.history.push('/category/women/20')}>
						Women{`'`}s fashion</section>
					<section className="image-home image-home-3" onClick={() =>
						this.props.history.push('/category/gear/3')}>Gear</section>
					<section className="image-home image-home-4" onClick={() =>
						this.props.history.push('/category/training/9')}>Training</section>
					<section className="image-home image-home-5" onClick={() =>
						this.props.history.push('/category/collections/7')}>
							Collections</section>
					<section className="image-home image-home-6" onClick={() =>
						this.props.history.push('/category/promotions/29')}>
							Promotions</section>
				</section>
			</article>
		);
	}
}

export default connect(mapStateToProps)(Home);
