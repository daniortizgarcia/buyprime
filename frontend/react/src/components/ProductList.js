import React from 'react';
import {Link} from 'react-router-dom';

/**
 * @class Product
 * @param {Object} props
 * @return {JSX}
 */
const ProductList = (props) => {
	const getImage = (array) => {
		let image;
		array.map((attr) => {
			if (attr.attribute_code === 'image') {
				image = attr.value;
			}
			return true;
		});
		return image;
	};

	return (
		props.products ?
			props.products.map((product, i) => {
				return product.price ? <section
					className="d-flex flex-column mt-3 align-items-center product"
					key={i}>
					<Link to={'/product/'+product.sku}>
						<img className="w-100 pl-3 pr-3 c-pointer"
							src={ process.env.REACT_APP_MAGENTO_MEDIA_URL+
								getImage(product.custom_attributes)}
							alt="thumblain product"/>
					</Link>
					<Link to={'/product/'+product.sku}>
						<p className="mb-2 c-pointer"><strong>{product.name}</strong></p>
					</Link>
					<p>{product.price} $</p>
					<button
						className="btn btn-primary mt-3 ml-4 mr-4 mb-3 invisible"
						type="button"
						disabled={props.buttAddDisabled}
						onClick={() =>
							props.addCart({
								id: product.id,
								name: product.name,
								sku: product.sku,
								price: product.price,
								image: getImage(product.custom_attributes),
								qty: 1})}>
						Add to Cart
					</button>
				</section> : '';
			})
			: <label className={`w-100 mt-5 mb-5 d-flex justify-content-center
				lds-dual-ring`}></label>
	);
};

export default ProductList;
