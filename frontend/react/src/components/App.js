import React from 'react';
import {connect} from 'react-redux';
import {Switch, Route} from 'react-router-dom';
import Header from './Header';
import Home from './Home';
import Video from './Video/Video';
import Login from './Auth/Login';
import Register from './Auth/Register';
import Cart from './Cart/Cart';
import PersonalData from './Checkout/PersonalData';
import Payment from './Checkout/Payment';
import Catalog from './Catalog/Catalog';
import SideBar from './SideBar';
import NotFound from './404';
import './App.css';

/**
 * @desc Import constants
 * @ignore
 */
import {
	APP_LOAD,
	APP_LOADU,
	SHOW_SIDEBAR,
	LOGOUT,
} from '../constants/actionTypes';
import agent from '../agent';

/**
 * mapStateToProps
 * @param {Object} state
 * @return {Object}
 * @ignore
 */
const mapStateToProps = (state) => {
	return {
		...state.common,
		idCart: state.cart.idCart,
		categoriesM: state.common.categories,
		categoriesMC: state.common.categories,
	};
};

/**
 * mapDispatchToProps
 * @param {*} dispatch
 * @param {*} props
 * @return {*}
 * @ignore
 */
const mapDispatchToProps = (dispatch, props) => ({
	/**
	 * onLoad
	 * @desc Charge the app
	 * @param {String} id
	 * @param {String} token
	 * @return {*}
	 * @ignore
	 */
	onLoad: (id) =>
		dispatch({type: APP_LOAD,
			payload: Promise.all([
				agent.Cart.idCart(id),
				agent.Products.getCategories(),
				agent.Products.getStreamProduct()])}),

	/**
	 * onLoadU
	 * @desc Charge the app
	 * @param {Integer | Boolean} idCart
	 * @return {*}
	 * @ignore
	 */
	onLoadU: (idCart) =>
		dispatch({type: APP_LOADU,
			payload: Promise.all([
				agent.Cart.guestCart(idCart),
				agent.Products.getCategories(),
				agent.Products.getStreamProduct()])}),

	/**
	 * showSidebar
	 * @param {String} state Boolean
	 * @desc Change stat SideBar
	 * @return {*}
	 * @ignore
	 */
	showSidebar: (state) =>
		dispatch({type: SHOW_SIDEBAR, state}),

	/**
	 * logout
	 * @desc Clear all information of the user
	 * @return {*}
	 * @ignore
	 */
	logout: () =>
		dispatch({type: LOGOUT, props}),
});

/**
 * @classdesc Class App
 */
class App extends React.Component {
	/**
	 * @constructor
	 */
	constructor() {
		super();
		this.state = {
			categoriesMC: '',
			categoriesMA: '',
		};
		/**
		 * changeLevel
		 * @desc Change the level of the Sidebar
		 * @param {Object} categories
		 */
		this.changeLevel = (categories) => {
			if (!categories) {
				this.setState({
					categoriesMC: this.state.categoriesMA,
					categoriesMA: this.props.categoriesM,
				});
			} else {
				this.setState({
					categoriesMC: categories,
					categoriesMA: this.state.categoriesMC,
				});
			}
		};
	}
	/**
	 * componentDidMount
	 * @desc Load the app
	 */
	componentDidMount() {
		if (this.props.userInfo) {
			this.props.onLoad(this.props.userInfo.id);
		} else {
			this.props.onLoadU(localStorage.getItem('idGCart') || false);
		}
		this.setState({
			categoriesMC: this.props.categoriesM,
		});
	}

	/**
	 * render
	 * @return {JSX}
	 */
	render() {
		if (this.props.appLoaded) {
			return (
				<div>
					<Header logout={this.props.logout} />
					<section className="d-flex fullHeight">
						{this.props.stateSidebar ?
							<section className="mb-0 section-sidebar"
								style={{'minWidth': '50px'}}></section> : ''}
						<SideBar
							showSidebar={this.props.showSidebar}
							stateSidebar={this.props.stateSidebar}
							categories={this.state.categoriesMC || this.props.categoriesM}
							changeLevel={this.changeLevel}/>
						<Switch>
							<Route exact path="/" component={() =>
								<Home history={this.props.history}
									onLoad={this.props.onLoad}/>} />
							<Route path="/video" component={Video} />
							<Route exact path="/login" component={Login} />
							<Route path="/register" component={Register} />
							<Route path="/cart" component={Cart} />
							<Route path="/personaldata" component={PersonalData} />
							<Route path="/payment" component={Payment}
								history={this.props.history}/>
							<Route path="/category/:name/:id" component={Catalog}
								key={this.props.location.pathname}/>
							<Route path="/product/:sku" component={Video}
								key={this.props.location.pathname}/>
							<Route component={NotFound} />
						</Switch>
					</section>
				</div>
			);
		}
		return (
			<div className="App">
			</div>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

