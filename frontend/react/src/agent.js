import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const superagent = superagentPromise(_superagent, global.Promise);

/**
 * API Request
 * @ignore
 */
const API_ROOT = 'http://localhost:8080/api';

const responseBody = (res) => res.body;

/**
 * Get, Post, Put, Del
 * @ignore
 */
const requests = {
	get: (url) =>
		superagent.get(`${API_ROOT}${url}`).then(responseBody),
	post: (url, body) =>
		superagent.post(`${API_ROOT}${url}`, body).then(responseBody),
};

const App = {
	init: () =>
		requests.get(`/app/init`),
};

const Products = {
	getProduct: (products) =>
		requests.post(`/products/getone`, {products: products}),
	getCategories: () =>
		requests.get(`/products/categories`),
	getStreamProduct: () =>
		requests.get(`/products/streamproduct`),
};

const Auth = {
	createCustomer: (user) =>
		requests.post(`/auth/register`,
			{customer: user.customer, password: user.password}),
	loginCustomer: (user, password) =>
		requests.post(`/auth/login`,
			{username: user, password: password}),
	infoUser: (token) =>
		requests.post(`/auth/infouser`, {token}).then((res) => res),
};

const Cart = {
	idCart: (id) =>
		requests.post(`/cart/idCart`, {id}),
	guestCart: (idCart) =>
		requests.post(`/cart/guestCart`, {idCart}),
	addItem: (sku, idCart) =>
		requests.post(`/cart/addItem`, {sku, idCart}),
	removeItem: (idItem, idCart) =>
		requests.post(`/cart/removeItem`, {idItem, idCart}),
	updateItem: (item) =>
		requests.post(`/cart/updateItem`, {item}),
};

const Checkout = {
	getCountries: () =>
		requests.get(`/checkout/countries`),
	shippingMethods: (methods, idCart) =>
		requests.post(`/checkout/shippingmethods`, {methods, idCart}),
	shippingAddress: (address, idCart) =>
		requests.post(`/checkout/shippingaddress`, {address, idCart}),
	fOrder: (payment, idCart) =>
		requests.post(`/checkout/forder`, {payment, idCart}),
};

const Catalog = {
	category: (id) =>
		requests.post(`/catalog/category`, {id}),
};

/**
* @desc Export constants
*/
export default {
	Auth,
	Products,
	Cart,
	Checkout,
	Catalog,
	App,
};
