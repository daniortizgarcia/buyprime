export const utils = {
	checkIfExist: (elem, param, othparam = undefined) => {
		if (elem) {
			if (othparam) {
				elem[othparam] = param;
			} else {
				elem = param;
			}
		}
		return elem;
	},
};
