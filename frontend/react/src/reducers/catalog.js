/**
 * @desc import constants
 */
import {
	LIST_CATALOG,
	UNMOUNT_CATALOG,
} from '../constants/actionTypes';

export default (state = {}, action) => {
	switch (action.type) {
	/**
	 * @desc Charge the app when the user is loggedin.
	 */
	case LIST_CATALOG:
		if (action.props.match.params.name !==
			action.payload.category.name.toLowerCase().replace(/ /g, '-')) {
			action.props.history.push('/');
			return {};
		}
		return {
			...state,
			category: action.payload.category,
			categories: action.payload.categories,
		};
	case UNMOUNT_CATALOG:
		return {};
	default:
		return state;
	}
};
