/**
 * @desc Import the constants
 */
import {
	INIT_CHAT,
	CHANGE_INPUT_CHAT,
	SAVE_MESSAGE,
	LIST_PRODUCT_STREAM,
	SET_PRODUCT_STREAM,
	SELECT_VIDEO_IMG,
	UNMOUNT_VIDEO,
} from '../constants/actionTypes';

/**
 * @desc Default state from Video.
 * @ignore
 */
const defaultState = {
	buttDisabled: true,
	buttAddDisabled: false,
};

export default (state = defaultState, action) => {
	switch (action.type) {
	/**
	 * @desc Get Messages and Channel info from Expressjs
	 */
	case INIT_CHAT:
		return {
			...state,
			channelName: action.channel,
			channelCover: action.cover,
			messages: action.messages.reverse(),
			buttDisabled: false,
		};
	/**
	 * @desc When cschange the value of input in the Chat
	 */
	case CHANGE_INPUT_CHAT:
		return {
			...state,
			message: action.message,
		};
	/**
	 * @desc Messages with new message added
	 */
	case SAVE_MESSAGE:
		state.messages.push(action.message);
		return {
			...state,
			messages: state.messages,
			message: '',
		};
	/**
	 * @desc Save the product to show in Video
	 */
	case LIST_PRODUCT_STREAM:
		if (action.props.match.params.sku) {
			if (action.props.match.params.sku !== action.payload.product[0].sku) {
				action.props.history.push('/');
				return {};
			}
		}
		return {
			...state,
			product: action.payload.product[0],
			videostream: false,
		};
	/**
	 * @desc Save the product to show in Video
	 */
	case SET_PRODUCT_STREAM:
		return {
			...state,
			product: action.product,
			videostream: true,
		};
	/**
	 * @desc Show image selected
	 */
	case SELECT_VIDEO_IMG:
		return {
			...state,
			imgSelected: action.img,
		};
	/**
	 * @desc Set default data component when unmount
	 */
	case UNMOUNT_VIDEO:
		return defaultState;
	default:
		return state;
	}
};
