import {createStore, applyMiddleware} from 'redux';
import {routerMiddleware} from 'react-router-redux';
import thunk from 'redux-thunk';
import reducer from './reducer';
import agent from './agent';
import {promiseMiddleware} from './middleware';
import {createBrowserHistory} from 'history';

export const history = createBrowserHistory();

// Build the middleware for intercepting and dispatching navigation actions
const myRouterMiddleware = routerMiddleware(history);

const saveLocal = () => {
	return new Promise((resolve) => {
		if (localStorage.getItem('token')) {
			agent.Auth.infoUser(localStorage.getItem('token'))
				.then((res) => {
					agent.App.init().then(() => {
						resolve(res.success);
					});
				});
		} else {
			agent.App.init().then(() => {
				resolve(false);
			});
		}
	});
};

export const storeF = async () => {
	let common = {
		appName: 'BuyPrime',
		stateSidebar: true,
	};
	const userinfo = await saveLocal();
	agent.App.init();

	if (userinfo) {
		common = {
			userInfo: userinfo.user,
			token: userinfo.token,
			appName: 'BuyPrime',
			stateSidebar: true,
		};
	} else {
		localStorage.removeItem('token');
	}
	return createStore(reducer,
		{common},
		applyMiddleware(myRouterMiddleware, promiseMiddleware, thunk),
	);
};

/* func();

export const store = createStore(reducer,
	{common: {userInfo: func()}}, composeWithDevTools(
		applyMiddleware(myRouterMiddleware, promiseMiddleware, thunk),
	)
);
*/
