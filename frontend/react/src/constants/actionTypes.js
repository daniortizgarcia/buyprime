/**
 * @desc ACTION TYPES
 */
export const ACTION_TYPES = 'ACTYON_TYPES';
export const APP_LOAD = 'APP_LOAD';
export const APP_LOADU = 'APP_LOADU';
export const ASYNC_START = 'ASYNC_START';
export const ASYNC_END = 'ASYNC_END';
export const SHOW_SIDEBAR = 'SHOW_SIDEBAR';
export const INIT_CHAT = 'INIT_CHAT';
export const CHANGE_INPUT_CHAT = 'CHANGE_INPUT_CHAT';
export const SAVE_MESSAGE = 'SAVE_MESSAGE';
export const UNMOUNT_VIDEO = 'UNMOUNT_VIDEO';
export const CHANGE_INPUT_AUTH = 'CHANGE_INPUT_AUTH';
export const CHANGE_TYPE_PASS = 'CHANGE_TYPE_PASS';
export const SHOW_ERRORS_LOGIN = 'SHOW_ERRORS_LOGIN';
export const SHOW_ERRORS_REGISTER = 'SHOW_ERRORS_REGISTER';
export const LOGIN = 'LOGIN';
export const REGISTER = 'REGISTER';
export const LOGOUT = 'LOGOUT';
export const USER_INFO ='USER_INFO';
export const UNMOUNT_REGISTER = 'UNMOUNT_REGISTER';
export const UNMOUNT_LOGIN = 'UNMOUNT_LOGIN';
export const LIST_PRODUCT_STREAM = 'LIST_PRODUCT_STREAM';
export const SET_PRODUCT_STREAM = 'SET_PRODUCT_STREAM';
export const SELECT_VIDEO_IMG = 'SELECT_VIDEO_IMG';
export const ADD_ITEM_CART = 'ADD_ITEM_CART';
export const REMOVE_ITEM_CART = 'REMOVE_ITEM_CART';
export const CHANGE_QTY_CART = 'CHANGE_QTY_CART';
export const CHANGE_INPUT_CHECKOUT = 'CHANGE_INPUT_CHECKOUT';
export const SHOW_ERRORS_CHECKOUT = 'SHOW_ERRORS_CHECKOUT';
export const FILL_IN_FIELDS = 'FILL_IN_FIELDS';
export const GET_COUNTRIES = 'GET_COUNTRIES';
export const SELECT_COUNTRY = 'SELECT_COUNTRY';
export const SELECT_PROVINCE = 'SELECT_PROVINCE';
export const SHIPPING_ADDRESS = 'SHIPPING_ADDRESS';
export const SHIPPING_METHODS = 'SHIPPING_METHODS';
export const PAY_METHODS = 'PAY_METHODS';
export const CHANGE_BUTTON_STATE = 'CHANGE_BUTTON_STATE';
export const LIST_CATALOG = 'LIST_CATALOG';
export const UNMOUNT_CATALOG = 'UNMOUNT_CATALOG';
export const LIST_SUBCATEGORIES = 'LIST_SUBCATEGORIES';
export const LIST_PRODUCT = 'LIST_PRODUCT';
export const SELECT_IMG = 'SELECT_IMG';
export const UNMOUNT_PRODUCT = 'UNMOUNT_PRODUCT';
export const TOASTR_OPTIONSL = {
	closeButton: true,
	preventDuplicated: true,
	positionClass: 'toast-top-left',
	closeDuration: 100,
};
export const TOASTR_OPTIONS = {
	closeButton: true,
	preventDuplicated: true,
	positionClass: 'toast-top-right',
	closeDuration: 100,
};
