import {combineReducers} from 'redux';
import common from './reducers/common';
import video from './reducers/video';
import auth from './reducers/auth';
import cart from './reducers/cart';
import checkout from './reducers/checkout';
import catalog from './reducers/catalog';

export default combineReducers({
	common,
	video,
	auth,
	cart,
	checkout,
	catalog,
});
