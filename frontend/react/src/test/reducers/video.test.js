/**
 * @desc Import Redcuer
 */
import video from '../../reducers/video';

/**
 * @desc Import action types
 */
import {
	INIT_CHAT,
	CHANGE_INPUT_CHAT,
	SAVE_MESSAGE,
	LIST_PRODUCT_STREAM,
	SET_PRODUCT_STREAM,
	SELECT_VIDEO_IMG,
	UNMOUNT_VIDEO,
} from '../../constants/actionTypes';

describe('Video Reducer', () => {
	it('Video initial state', () => {
		expect(video(undefined, {
			buttDisabled: true,
			buttAddDisabled: false,
		})).toEqual({
			buttDisabled: true,
			buttAddDisabled: false,
		});
	});

	it('Video INIT_CHAT', () => {
		expect(video({}, {
			type: INIT_CHAT,
			channel: 'Devopensource',
			cover: 'Cover',
			test: true,
			messages: ['message1', 'message2'],
		})).toEqual({
			channelName: 'Devopensource',
			channelCover: 'Cover',
			messages: ['message2', 'message1'],
			buttDisabled: false,
		});
	});

	it('Video CHANGE_INPUT_CHAT', () => {
		expect(video({}, {
			type: CHANGE_INPUT_CHAT,
			message: 'message',
		})).toEqual({
			message: 'message',
		});
	});

	it('Video SAVE_MESSAGE', () => {
		expect(video({
			messages: [],
		}, {
			type: SAVE_MESSAGE,
			message: 'message1',
		})).toEqual({
			messages: ['message1'],
			message: '',
		});
	});

	it('Video LIST_PRODUCT_STREAM', () => {
		expect(video({}, {
			type: LIST_PRODUCT_STREAM,
			payload: {
				product: [{sku: 'sku1'}],
			},
			props: {
				match: {params: {sku: 'sku1'}},
			},
		})).toEqual({
			product: {sku: 'sku1'},
			videostream: false,
		});

		expect(video({}, {
			type: LIST_PRODUCT_STREAM,
			payload: {
				product: [{sku: 'sku1'}],
			},
			props: {
				match: {params: {sku: false}},
			},
		})).toEqual({
			product: {sku: 'sku1'},
			videostream: false,
		});

		expect(video({}, {
			type: LIST_PRODUCT_STREAM,
			payload: {
				product: [{sku: 'sku12'}],
			},
			props: {
				match: {params: {sku: 'sku1'}},
				history: {push: (url) => {}},
			},
		})).toEqual({});
	});

	it('Video SET_PRODUCT_STREAM', () => {
		expect(video({}, {
			type: SET_PRODUCT_STREAM,
			product: {id: 1, name: 'product', sku: 'sku'},
		})).toEqual({
			product: {id: 1, name: 'product', sku: 'sku'},
			videostream: true,
		});
	});

	it('Video SELECT_VIDEO_IMG', () => {
		expect(video({}, {
			type: SELECT_VIDEO_IMG,
			img: 'img',
		})).toEqual({
			imgSelected: 'img',
		});
	});

	it('Video UNMOUNT_VIDEO', () => {
		expect(video({videostream: false}, {
			type: UNMOUNT_VIDEO,
		})).toEqual({
			buttDisabled: true,
			buttAddDisabled: false,
		});
	});
});
