/**
 * @desc Import Redcuer
 */
import catalog from '../../reducers/catalog';

/**
 * @desc Import action types
 */
import {
	LIST_CATALOG,
	LIST_SUBCATEGORIES,
	UNMOUNT_CATALOG,
} from '../../constants/actionTypes';

describe('Catalog Reducer', () => {
	it('Catalog initial state', () => {
		expect(catalog(undefined, {})).toEqual({});
	});

	it('Catalog LIST_CATALOG', () => {
		expect(catalog(undefined, {
			type: LIST_CATALOG,
			payload: {
				category: {name: 'Categoria'},
				categories: [{items: ['item1', 'item2']}, {items: ['item1']}],
			},
			props: {
				match: {
					params: {
						name: 'categoria',
					},
				},
			},
		})).toEqual({
			category: {name: 'Categoria'},
			categories: [{items: ['item1', 'item2']}, {items: ['item1']}],
		});

		expect(catalog(undefined, {
			type: LIST_CATALOG,
			payload: {
				category: {name: 'Categoriaaa'},
				categories: [{items: ['item1', 'item2']}, {items: ['item1']}],
			},
			props: {
				match: {
					params: {
						name: 'categoria',
					},
				},
				history: {push: (url) => {}},
			},
		})).toEqual({});
	});

	it('Catalog UNMOUNT_CATALOG', () => {
		expect(catalog(undefined, {
			type: UNMOUNT_CATALOG,
		})).toEqual({});
	});
});
