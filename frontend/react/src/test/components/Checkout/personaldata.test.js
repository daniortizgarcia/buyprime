import React from 'react';
import PersonalData from '../../../components/Checkout/PersonalData';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

import {JSDOM} from 'jsdom';
const dom = new JSDOM();
global.document = dom.window.document;
const mockStore = configureStore();
let rendered;
let instance = '';
const cartItems = [{
	item_id: 223,
	sku: 'VA22-SI-NA',
	qty: 1,
	name: 'Silver Amor Bangle Set',
	price: 98,
	product_type: 'simple',
	quote_id: '93',
	image: {
		id: 21,
		media_type: 'image',
		label: 'Image',
		position: 1,
		disabled: false,
		types: [
			'image',
			'small_image',
			'thumbnail',
		],
		file: '/v/a/va22-si_main.jpg',
	},
}, {
	item_id: 223,
	sku: 'VA22-SI-NA',
	qty: 3,
	name: 'Silver Amor Bangle Set',
	price: 98,
	product_type: 'simple',
	quote_id: '93',
	image: {
		id: 21,
		media_type: 'image',
		label: 'Image',
		position: 1,
		disabled: false,
		types: [
			'image',
			'small_image',
			'thumbnail',
		],
		file: '/v/a/va22-si_main.jpg',
	},
}];
const props = {
	checkout: {
		shipAddress: 'shipaddress',
		inputShipMethod: 1,
		shipMethods: [
			{method_title: 'mtitle1', carrier_title: 'ctitle1', method_code: 1},
			{method_title: 'mtitle2', carrier_title: 'ctitle2', method_code: 2},
		],
		email: '',
		fname: '',
		lname: '',
		company: '',
		street: '',
		city: '',
		country: '',
		provinceId: '',
		postalcode: '',
		phone: '',
		provinces: [],
		history: {push: (param) => {}},
		errorsCheckout: [{key: 'Error', error: 'error'}],
		countries: [{id: 1, full_name_english: 'Spain'}],
	},
	common: {
		userInfo: {
			email: 'dani@gmail.com',
		},
	},
	cart: {
		idCart: 1,
		cartItems: cartItems,
		buttAddDisabled: true,
	},
};

const store = mockStore(props);
rendered = renderer.create(
	<Provider store={store}><Router><PersonalData /></Router></Provider>
);
instance = rendered.root;
describe('Personal Component', () => {
	it('Check the props, if returned JSX is correctly and call all the functions',
		() => {
			instance.findAllByType('input')[0].props.onChange({target: {value: 'd'}});
			instance.findAllByType('form')[0].props.onSubmit(
				{preventDefault: () => {}});
			instance.findAllByType('form')[1].props.onSubmit(
				{preventDefault: () => {}});
			instance.findAllByType('select')[0].props.onChange(
				{target: {value: 'd'}});
			instance.findAllByType('select')[1].props.onChange(
				{target: {value: 'd'}});
			instance.findAllByProps({className: 'form-check-input'})[0]
				.props.onChange({target: {value: 'd'}});
		}
	);

	it('All possibles situations', () => {
		props.cart.cartItems = [];
		props.checkout.email = 'dani@gmail.com';
		props.checkout.fname = 'Dani';
		props.checkout.lname = 'Ortiz';
		props.checkout.company = 'Devopensource';
		props.checkout.street = 'Street';
		props.checkout.city = 'Alcoy';
		props.checkout.country = 'Spain';
		props.checkout.provinceId = 1;
		props.checkout.postalcode = 46870;
		props.checkout.phone = 612312312;
		props.checkout.provinces = ['prov1', 'prov2'];
		const store = mockStore(props);
		rendered = renderer.create(
			<Provider store={store}>
				<Router><PersonalData /></Router></Provider>
		);
		instance = rendered.root;
		instance.findAllByType('form')[0].props.onSubmit(
			{preventDefault: () => {}});
		instance.findAllByType('form')[1].props.onSubmit(
			{preventDefault: () => {}});
	});

	it('All possibles situations', () => {
		props.cart.cartItems = cartItems;
		props.common.userInfo = undefined;
		props.checkout.shipMethods = undefined;
		props.checkout.countries = undefined;
		const store = mockStore(props);
		rendered = renderer.create(
			<Provider store={store}>
				<Router><PersonalData /></Router></Provider>
		);
		instance = rendered.root;
		instance.findAllByType('form')[0].props.onSubmit(
			{preventDefault: () => {}});
		instance.findAllByType('form')[1].props.onSubmit(
			{preventDefault: () => {}});
	});

	it('All possibles situations', () => {
		props.checkout.provinces = undefined;
		const store = mockStore(props);
		rendered = renderer.create(
			<Provider store={store}>
				<Router><PersonalData /></Router></Provider>
		);
		instance = rendered.root;
		instance.findAllByType('form')[0].props.onSubmit(
			{preventDefault: () => {}});
		instance.findAllByType('form')[1].props.onSubmit(
			{preventDefault: () => {}});
	});
});
