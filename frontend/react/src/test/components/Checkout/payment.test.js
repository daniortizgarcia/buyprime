import React from 'react';
import Payment from '../../../components/Checkout/Payment';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

const mockStore = configureStore();
let rendered;
let instance = '';

const props = {
	checkout: {
		inputPayMethod: 'paymethod',
		history: {push: (param) => {}},
		shipMethods: 'method1',
		payMethods: [
			{code: 1, title: 'method1'},
			{code: 2, title: 'method2'},
		],
		inputPayMethod: 1,
		totals: {
			subtotal: 2,
			shipping_amount: 1,
			grand_total: 3,
			items: [{}, {}],
		},
	},
	cart: {
		idCart: 1,
		cartItems: [{
			item_id: 223,
			sku: 'VA22-SI-NA',
			qty: 1,
			name: 'Silver Amor Bangle Set',
			price: 98,
			product_type: 'simple',
			quote_id: '93',
			image: {
				id: 21,
				media_type: 'image',
				label: 'Image',
				position: 1,
				disabled: false,
				types: [
					'image',
					'small_image',
					'thumbnail',
				],
				file: '/v/a/va22-si_main.jpg',
			},
		}, {
			item_id: 223,
			sku: 'VA22-SI-NA',
			qty: 3,
			name: 'Silver Amor Bangle Set',
			price: 98,
			product_type: 'simple',
			quote_id: '93',
			image: {
				id: 21,
				media_type: 'image',
				label: 'Image',
				position: 1,
				disabled: false,
				types: [
					'image',
					'small_image',
					'thumbnail',
				],
				file: '/v/a/va22-si_main.jpg',
			},
		}],
		buttAddDisabled: true,
	},
};

const store = mockStore(props);
rendered = renderer.create(
	<Provider store={store}><Router><Payment /></Router></Provider>
);
instance = rendered.root;
describe('Payment Component', () => {
	it('Check the props, if returned JSX is correctly and call all the functions',
		() => {
			instance.findAllByType('input')[0].props.onChange({target: {value: 'd'}});
			instance.findByType('form').props.onSubmit({preventDefault: () => {}});
		}
	);

	it('All possibles situations', () => {
		props.checkout.payMethods = false;
		props.checkout.shipMethods = false;
		const store = mockStore(props);
		rendered = renderer.create(
			<Provider store={store}>
				<Router><Payment /></Router></Provider>
		);
		instance = rendered.root;
	});

	it('All possibles situations', () => {
		props.cart.cartItems = [];
		const store = mockStore(props);
		rendered = renderer.create(
			<Provider store={store}>
				<Router><Payment /></Router></Provider>
		);
		instance = rendered.root;
	});

	it('All possibles situations', () => {
		props.cart.cartItems = [];
		props.checkout.totals = undefined;
		const store = mockStore(props);
		rendered = renderer.create(
			<Provider store={store}>
				<Router><Payment /></Router></Provider>
		);
		instance = rendered.root;
	});
});
