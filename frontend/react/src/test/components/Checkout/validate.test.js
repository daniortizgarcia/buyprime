import {validate} from '../../../components/Checkout/validate';

describe('Validate Functions', () => {
	it('validateFormCheck', async () => {
		expect(await validate.validateFormCheck(
			'', '', '', '', '', '', '', '', '', '', [{}])).toEqual([
			{key: 'Email', error: 'Email is required'},
			{key: 'First Name', error: 'First Name is required'},
			{key: 'Last Name', error: 'Last Name is required'},
			{key: 'Street', error: 'Street is required'},
			{key: 'City', error: 'City is required'},
			{key: 'Country', error: 'Select a Country'},
			{key: 'Province', error: 'Select a Province'},
			{key: 'Zip/Postal Code', error: 'Zip/Postal Code is required'},
			{key: 'Phone', error: 'Phone is required'},
		]);
		expect(await validate.validateFormCheck(
			'dani', 'Da ', 'Or ', 'De', 'st ', 'Al ', 'Spain',
			'Alicante', '4678', '61231231', [])).toEqual([
			{key: 'Email', error: 'Email is invalid'},
			{key: 'First Name',
				error: 'First Name can\'t contains spaces'},
			{key: 'Last Name', error: 'Last Name can\'t contains spaces'},
			{key: 'Company',
				error: 'Company must be between 3 & 15 letters'},
			{key: 'Street', error: 'Street can\'t contains spaces'},
			{key: 'City', error: 'City can\'t contains spaces'},
			{key: 'Country', error: 'Invalid Country'},
			{key: 'Zip/Postal Code', error: 'Zip/Postal Code is invalid'},
			{key: 'Phone', error: 'Phone is invalid'}]);
		expect(await validate.validateFormCheck(
			'dani@gmail.com', 'Da', 'Or', 'Devopensource', 'st', 'Al', 'Spain',
			'', '46870', '612312312', [{}, {}])).toEqual([
			{key: 'First Name',
				error: 'First Name must be between 3 & 15 letters'},
			{key: 'Last Name',
				error: 'Last Name must be between 3 & 15 letters'},
			{key: 'Street',
				error: 'Street must be between 3 & 15 letters'},
			{key: 'City', error: 'City must be between 3 & 15 letters'},
			{key: 'Province', error: 'Select a Province'},
		]);
		expect(await validate.validateFormCheck(
			'dani@gmail.com', 'Dani', 'Ortiz', 'Devopensource', 'street', 'Alcoy',
			'Spain', 'Alicante', '46870', '612312312', [{}, {}])).toEqual([]);
	});
});
