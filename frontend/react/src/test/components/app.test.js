import React from 'react';
import App from '../../components/App';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

const mockStore = configureStore();
let rendered;
let instance = '';

const props = {
	common: {
		appLoaded: true,
		categories: [
			{level: 3, id: 1, name: 'Cate gory', children_data: [
				{level: 2, id: 2, name: 'Cate gory', children_data: []},
				{level: 2, id: 2, name: 'Cate gory', children_data: []},
			]},
			{level: 2, id: 2, name: 'Cate gory', children_data: []},
		],
		userInfo: {id: 1},
		location: {pathname: '/'},
		showSidebar: true,
		stateSidebar: true,
	},
	cart: {
		idCart: 1,
		cartItems: [],
	},
};
const store = mockStore(props);
rendered = renderer.create(
	<Provider store={store}><Router><App /></Router></Provider>
);
instance = rendered.root;
describe('Home Component', () => {
	it('Check the props, if returned JSX is correctly and call all the functions',
		() => {
			expect(instance.props.store.getState()).toEqual(props);
			instance.findByProps({
				className: `fas fa-chevron-left p-3 c-pointer border-bottom
						d-flex  align-items-center`,
			}).props.onClick();
			instance.findAllByProps({
				className: 'fas fa-chevron-right p-2 c-pointer',
			})[0].props.onClick();
			instance.findAllByProps({
				className: 'p-1 text-center border-bottom changeSideBar c-pointer',
			})[0].props.onClick();
			instance.findByProps({
				className: 'nav-item ml-2 mr-2 li-logout',
			}).props.onClick();
		}
	);

	it('All possibles situations', () => {
		props.common.stateSidebar = false;
		const store = mockStore(props);
		rendered = renderer.create(
			<Provider store={store}><Router><App /></Router></Provider>
		);
	});

	it('All possibles situations', () => {
		props.common.appLoaded = false;
		props.common.userInfo = false;
		props.common.stateSidebar = false;
		const store = mockStore(props);
		rendered = renderer.create(
			<Provider store={store}><Router><App /></Router></Provider>
		);
	});
});
