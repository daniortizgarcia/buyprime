import React from 'react';
import Header from '../../components/Header';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

const mockStore = configureStore();
let rendered = '';
let instance = '';

const props = {
	cart: {
		cartItems: [{
			qty: 1,
		}, {
			qty: 2,
		}, {
			qty: 1,
		}],
	},
	common: {
		userInfo: {id: 1},
		streamProduct: {id: 1, name: 'product', sku: 'sku'},
	},
};
const props2 = {
	cart: {
		cartItems: [{
			qty: 1,
		}],
	},
	common: {
		userInfo: undefined,
	},
};
const props3 = {
	cart: {
		cartItems: [],
	},
	common: {
		userInfo: {id: 1},
	},
};
const store = mockStore(props);
const logout = () => {};
rendered = renderer.create(
	<Provider store={store}><Router>
		<Header logout={logout}/>
	</Router></Provider>
);
instance = rendered.root;
describe('Header and Menu Component', () => {
	it('Check the props, if returned JSX is correctly', () => {
		expect(instance.props.store.getState()).toEqual(props);
		expect(instance.props.children.props.children.props)
			.toEqual({logout: logout});
		expect(instance.findAllByType('nav').length).toBeGreaterThan(0);
	});

	it('All possibles situations', () => {
		let store = mockStore(props2);
		rendered = renderer.create(
			<Provider store={store}><Router>
				<Header logout={logout}/>
			</Router></Provider>
		);
		instance = rendered.root;

		store = mockStore(props3);
		rendered = renderer.create(
			<Provider store={store}><Router>
				<Header logout={logout}/>
			</Router></Provider>
		);
		instance = rendered.root;

		instance.findByProps({
			className: 'nav-item ml-2 mr-2 li-logout',
		}).props.onClick();
	});
});
