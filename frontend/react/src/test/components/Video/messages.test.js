import React from 'react';
import Messages from '../../../components/Video/Messages';
import renderer from 'react-test-renderer';

let rendered = '';
let instance = '';

const props = {
	channel: () => {},
	saveMessages: () => {},
	changeInput: () => {},
	cover: 'cover',
	messages: [
		{_sender: {userId: 'nickname', nickname: 'Nickname'}, message: 'messsage'},
		{_sender: {userId: 'nickname', nickname: 'Nickname'}, message: 'messsage'},
	],
	channel: 'Channel',
	nickname: 'nickname',
	buttDisabled: false,
};

rendered = renderer.create(<Messages {...props}/>);
instance = rendered.root;

describe('Messages and MessagesList Component', () => {
	it('Check the props, if returned JSX is correctly', () => {
		expect(instance.props).toEqual(props);
		expect(instance.findByType('button').props.children).toEqual('Chat');
		expect(instance.findAllByType('strong').length).toBeGreaterThan(1);
	});

	it('All possibles situations', () => {
		props.channel = undefined;
		props.messages = undefined;
		props.nickname = undefined;
		rendered = renderer.create(<Messages {...props}/>);
		instance = rendered.root;
	});
});
