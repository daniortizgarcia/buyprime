import React from 'react';
import Video from '../../../components/Video/Video';
import {Provider} from 'react-redux';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

const mockStore = configureStore();
let rendered;
let instance = '';

const props = {
	video: {
		message: 'message',
		product: {
			id: 1,
			name: 'producto1',
			sku: 'sku',
			price: 10,
			custom_attributes: [{
				attribute_code: 'video_link',
				value: 'att_value',
			}, {
				attribute_code: 'description',
				value: 'att_value',
			}],
			media_gallery_entries: [
				{file: 'file'},
			],
		},
		imgSelected: true,
		channelName: 'Name',
		channelCover: 'Cover',
		messages: [
			{_sender: {userId: 'nickname', nickname: 'Nick'}, message: 'messsage'},
			{_sender: {userId: 'nickname', nickname: 'Nick'}, message: 'messsage'},
		],
		buttDisabled: false,
		history: {push: (param) => param},
	},
	common: {
		userInfo: {
			email: 'dani@gmail.com',
			firstname: 'Dani',
			lastname: 'Ortiz',
		},
		nickname: 'DaniOrtiz',
	},
	cart: {
		idCart: 1,
		buttAddDisabled: false,
	},
};
const props2 = {match: {params: {sku: 'sku'}}};
const store = mockStore(props);
rendered = renderer.create(
	<Provider store={store}><Video {...props2}/></Provider>
);
instance = rendered.root;
describe('Video Component', () => {
	it('Check the props, if returned JSX is correctly and call all the functions',
		() => {
			expect(instance.props.store.getState()).toEqual(props);
			instance.findAllByType('button')[0].props.onClick();
			instance.findAllByType('img')[1].props.onClick();
			instance.findByType('i').props.onClick();
			instance.findByType('textarea').props.onChange({target: {value: 'd'}});
			instance.findByType('form').props.onSubmit({preventDefault: () => {}});
			rendered.unmount();
		}
	);

	it('All possibles situations', () => {
		props2.match.params = {};
		props.cart.buttAddDisabled = true;
		const store = mockStore(props);
		rendered = renderer.create(
			<Provider store={store}><Video {...props2}/></Provider>
		);
		instance = rendered.root;
		instance.findAllByType('button')[0].props.onClick();
		instance.findAllByType('img')[1].props.onClick();
		instance.findByType('i').props.onClick();
		instance.findByType('textarea').props.onChange({target: {value: 'd'}});
		instance.findByType('form').props.onSubmit({preventDefault: () => {}});
		rendered.unmount();
	});

	it('All possibles situations', () => {
		props.video.product = undefined;
		props.common.streamProduct = true;
		props.common.userInfo = undefined;
		const store = mockStore(props);
		rendered = renderer.create(
			<Provider store={store}><Video {...props2}/></Provider>
		);
		instance = rendered.root;
		rendered.unmount();
	});
});
