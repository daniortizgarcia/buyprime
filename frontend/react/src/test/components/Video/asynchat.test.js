import {asynchat} from '../../../components/Video/asynchat';
import SendBird from 'sendbird';
const sb = new SendBird({appId: process.env.REACT_APP_SENDBIRD_ID});
let channel = '';

describe('Asynchat Functions', () => {
	it('connect', async () => {
		expect(await asynchat.connect('DaniOrtiz', 'dani', sb)).toEqual(true);
		expect(await asynchat.connect('DaniOrtiz', '', sb))
			.toEqual('SendBirdException: Invalid parameter.');
	});

	it('searchchannel', async () => {
		const openChannelListQuery = sb.OpenChannel.createOpenChannelListQuery();
		openChannelListQuery.nameKeyword = 'producto1';
		expect(await asynchat.searchchannel(openChannelListQuery)).toEqual(
			'sendbird_open_channel_49090_25e32c6fef301e1db600381a9a4b952372e414a0');
		openChannelListQuery.nameKeyword = 'producto2';
		expect(await asynchat.searchchannel(openChannelListQuery)).toEqual(false);
	});

	it('createchannel', async () => {
		// expect(await asynchat.createchannel('producto1', '', sb)).toContain(
		//	'sendbird_open_channel');
	});

	it('getchannel', async () => {
		channel = await asynchat.getchannel(sb, () => {},
			'sendbird_open_channel_49090_25e32c6fef301e1db600381a9a4b952372e414a0',
			'sku1');
		expect(await asynchat.getchannel(sb, () => {},
			'sendbird_open_channel_49090_25e32c6fef301e1db600381a9a4b952372e414a0',
			'sku1')).toEqual(channel);
	});

	it('messagereceived', async () => {
		/* eslint-disable */
		expect(await asynchat.messagereceived(channel, () => {}, this)).toEqual(true);
		/* eslint-enable */
	});

	it('exit', async () => {
		expect(await asynchat.exit(channel)).toEqual(true);
		channel = {
			exit: (func) => {
				return func(false, true);
			},
		};
		expect(await asynchat.exit(channel)).toEqual(false);
	});
});
