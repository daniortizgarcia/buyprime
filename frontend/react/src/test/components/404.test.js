import React from 'react';
import NotFound from '../../components/404';
import renderer from 'react-test-renderer';

const rendered = renderer.create(<NotFound />);
const instance = rendered.root;

describe('404 Component', () => {
	it('Check if exist in JSX section and de className of section', () => {
		expect(instance.findAllByType('section').length).toEqual(1);
		expect(instance.findByProps({className: 'w-50 not-found'})
			.props.children).toEqual(undefined);
		expect(instance.findAllByType('label').length).toEqual(1);
	});
});
