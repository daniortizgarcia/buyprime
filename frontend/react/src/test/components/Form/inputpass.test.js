import React from 'react';
import {InputPass} from '../../../components/Form/InputPass';
import renderer from 'react-test-renderer';

let rendered = '';
let instance = '';

// const mockStore = configureStore();
const props = {
	name: 'password',
	labelName: 'Password',
	passType: 'password',
	storeName: 'name',
	placeholder: 'Enter a Password',
	value: '',
	passName: 'passwordTest',
	changeInput: (param) => param,
	changePassType: (param) => param,
};

rendered = renderer.create(<InputPass {...props}/>);
instance = rendered.root;

describe('InputPass Component', () => {
	it('Check the props, if returned JSX is correctly', () => {
		expect(instance.props).toEqual(props);
		expect(instance.findAllByType('input').length).toBeGreaterThan(0);
		instance.findByType('i').props.onClick();
		expect(instance.findByProps({className: 'mb-0 labelName'}).children)
			.toContain('Password');
	});
	it('All possibles situations', () => {
		props.passType = 'text';
		rendered = renderer.create(<InputPass {...props}/>);
		instance = rendered.root;
		instance.findByType('i').props.onClick();
	});
});
