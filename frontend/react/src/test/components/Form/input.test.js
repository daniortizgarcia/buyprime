import React from 'react';
import {Input} from '../../../components/Form/Input';
import renderer from 'react-test-renderer';

let rendered = '';
let instance = '';

// const mockStore = configureStore();
const props = {
	name: 'name',
	labelName: 'Name',
	type: 'text',
	storeName: 'name',
	placeholder: 'Your Name',
	value: '',
	changeInput: (param) => param,
};
rendered = renderer.create(<Input {...props}/>);
instance = rendered.root;

describe('Input Component', () => {
	it('Check the props, if returned JSX is correctly', () => {
		expect(instance.props).toEqual(props);
		expect(instance.findByProps({className: 'mb-0 labelName'}).children)
			.toContain('Name');
	});
});
