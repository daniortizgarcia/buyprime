import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import ProductList from '../../components/ProductList';
import renderer from 'react-test-renderer';

let rendered = '';
let instance = '';

let props = {
	products: [
		{
			id: 1,
			sku: 'sku1',
			name: 'prod1',
			price: 2,
			custom_attributes: [
				{attribute_code: 'image', value: 'value'},
				{attribute_code: 'notimage', value: 'value'},
			],
		},
		{
			id: 2,
			sku: 'sku2',
			name: 'prod2',
			price: 5,
			custom_attributes: [
				{attribute_code: 'image', value: 'value'},
				{attribute_code: 'notimage', value: 'value'},
			],
		},
		{
			id: 2,
			sku: 'sku2',
			name: 'prod2',
			price: 0,
			custom_attributes: [
				{attribute_code: 'image', value: 'value'},
				{attribute_code: 'notimage', value: 'value'},
			],
		},
	],
	buttAddDisabled: false,
	addCart: (prod) => {},
};

rendered = renderer.create(<Router><ProductList {...props} /></Router>);
instance = rendered.root;

describe('ProductList Component', () => {
	it('Check the props, if returned JSX is correctly and call all the functions',
		() => {
			expect(instance.props.children.props).toEqual(props);
			expect(instance.findAllByType('button').length).toBeGreaterThan(0);
			expect(instance.findAllByProps({
				className: 'd-flex flex-column mt-3 align-items-center product',
			}).length).toBeGreaterThan(0);
			expect(instance.findAllByProps({
				className: 'btn btn-primary mt-3 ml-4 mr-4 mb-3 invisible',
			})[0].children).toContain('Add to Cart');
			instance.findAllByType('button')[0].props.onClick();
		}
	);

	it('All possibles situations', () => {
		props = {};
		rendered = renderer.create(<Router><ProductList {...props} /></Router>);
	});
});
