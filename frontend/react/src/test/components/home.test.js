import React from 'react';
import Home from '../../components/Home';
import {Provider} from 'react-redux';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

const mockStore = configureStore();
let rendered;
let instance = '';

const props = {
	cart: {
		idCart: 1,
	},
	common: {
		userInfo: {id: 1},
	},
	history: {push: () => {}},
};
const props2 = {
	cart: {
		idCart: undefined,
	},
	common: {
		userInfo: {id: 1},
	},
};
const store = mockStore(props);
const onLoad =(id) => id;
const push = () => {};
rendered = renderer.create(
	<Provider store={store}><Home onLoad={onLoad}
		history={{push}} /></Provider>
);
instance = rendered.root;
describe('Home Component', () => {
	it('Check the props, if returned JSX is correctly', () => {
		expect(instance.props.store.getState()).toEqual(props);
		expect(instance.props.children.props).toEqual({
			onLoad: onLoad, history: {push}});
		expect(instance.findAllByType('article').length).toBeGreaterThan(0);
		for (let i = 1; i < 7; i++) {
			const classKey = 'image-home image-home-'+i;
			instance.findByProps({className: classKey}).props.onClick();
		}
	});

	it('All possibles situations', () => {
		const store = mockStore(props2);
		rendered = renderer.create(
			<Provider store={store}><Home onLoad={(id) => id}
				history={{push}} /></Provider>
		);
	});
});
