import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import SideBar from '../../components/SideBar';
import renderer from 'react-test-renderer';

let rendered = '';
let instance = '';

const props = {
	stateSidebar: true,
	showSidebar: (val) => {},
	categories: [
		{level: 3, id: 1, name: 'cat1', children_data: [{}, {}]},
		{level: 2, id: 2, name: 'cat2', children_data: []},
	],
	changeLevel: () => {},
};

rendered = renderer.create(<Router><SideBar {...props} /></Router>);
instance = rendered.root;

describe('Sidebar Component', () => {
	it('Check the props, if returned JSX is correctly and call all the functions',
		() => {
			expect(instance.props.children.props).toEqual(props);
			expect(instance.findAllByType('i').length).toBeGreaterThan(0);
			expect(instance.findAllByProps({
				className: 'fas fa-caret-left',
			}).length).toBeGreaterThan(0);
			expect(instance.findByProps({
				className: 'p-2 text-center border-bottom small popularChannels',
			}).children).toContain('Populars Channels');
			instance.findByProps({
				className: 'p-1 text-center border-bottom changeSideBar c-pointer',
			}).props.onClick();
			instance.findByProps({
				className: `fas fa-chevron-left p-3 c-pointer border-bottom
						d-flex  align-items-center`,
			}).props.onClick();
			instance.findByProps({
				className: 'fas fa-chevron-right p-2 c-pointer',
			}).props.onClick();
		}
	);

	it('All possibles situations', () => {
		props.categories[0] = {
			level: 2, id: 1, name: 'cat1', children_data: [{}, {}],
		};
		rendered = renderer.create(<Router><SideBar {...props} /></Router>);
	});

	it('All possibles situations', () => {
		props.categories = false;
		rendered = renderer.create(<Router><SideBar {...props} /></Router>);
	});

	it('All possibles situations', () => {
		props.stateSidebar = false;
		rendered = renderer.create(<Router><SideBar {...props} /></Router>);
		instance = rendered.root;
		expect(instance.findAllByProps({
			className: 'fas fa-caret-right',
		}).length).toBeGreaterThan(0);
		instance.findByProps({
			className: 'p-1 text-center border-bottom changeSideBar c-pointer',
		}).props.onClick();
	});
});
