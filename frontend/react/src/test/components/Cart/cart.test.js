import React from 'react';
import Cart from '../../../components/Cart/Cart';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

const mockStore = configureStore();
let rendered;
let instance = '';

const props = {
	cart: {
		idCart: 1,
		cartItems: [{
			item_id: 223,
			sku: 'VA22-SI-NA',
			qty: 1,
			name: 'Silver Amor Bangle Set',
			price: 98,
			product_type: 'simple',
			quote_id: '93',
			image: {
				id: 21,
				media_type: 'image',
				label: 'Image',
				position: 1,
				disabled: false,
				types: [
					'image',
					'small_image',
					'thumbnail',
				],
				file: '/v/a/va22-si_main.jpg',
			},
		}, {
			item_id: 223,
			sku: 'VA22-SI-NA',
			qty: 3,
			name: 'Silver Amor Bangle Set',
			price: 98,
			product_type: 'simple',
			quote_id: '93',
			image: {
				id: 21,
				media_type: 'image',
				label: 'Image',
				position: 1,
				disabled: false,
				types: [
					'image',
					'small_image',
					'thumbnail',
				],
				file: '/v/a/va22-si_main.jpg',
			},
		}],
		buttAddDisabled: true,
	},
};
const props2 = {
	cart: {
		idCart: 1,
		cartItems: [{
			item_id: 223,
			sku: 'VA22-SI-NA',
			qty: 1,
			name: 'Silver Amor Bangle Set',
			price: 98,
			product_type: 'simple',
			quote_id: '93',
			image: {
				id: 21,
				media_type: 'image',
				label: 'Image',
				position: 1,
				disabled: false,
				types: [
					'image',
					'small_image',
					'thumbnail',
				],
				file: '/v/a/va22-si_main.jpg',
			},
		}],
		buttAddDisabled: true,
	},
};

const store = mockStore(props);
rendered = renderer.create(
	<Provider store={store}>
		<Router><Cart match={{params: {id: 1}}} /></Router></Provider>
);
instance = rendered.root;
describe('Catalog Component', () => {
	it('Check the props, if returned JSX is correctly and call all the functions',
		() => {
			instance.findAllByType('i')[0].props.onClick();
			instance.findAllByType('label')[1].props.onClick();
			instance.findAllByType('label')[2].props.onClick();
		}
	);

	it('All possibles situations', () => {
		props.cart.cartItems = [];
		const store = mockStore(props2);
		rendered = renderer.create(
			<Provider store={store}>
				<Router><Cart match={{params: {id: 1}}} /></Router></Provider>
		);
		instance = rendered.root;
	});

	it('All possibles situations', () => {
		props.cart.cartItems = [];
		const store = mockStore(props);
		rendered = renderer.create(
			<Provider store={store}>
				<Router><Cart match={{params: {id: 1}}} /></Router></Provider>
		);
		instance = rendered.root;
	});
});
