import React from 'react';
import Catalog from '../../../components/Catalog/Catalog';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

const mockStore = configureStore();
let rendered;
let instance = '';

const props = {
	cart: {
		idCart: 1,
		buttAddDisabled: true,
	},
	catalog: {
		category: {
			level: 2,
			id: 1,
			name: 'cat1',
			children_data: [{}, {}],
			children: '1',
		},
		categories: [
			{name: 'prod1', items: [
				{
					id: 1,
					sku: 'sku1',
					name: 'prod1',
					price: 2,
					custom_attributes: [
						{attribute_code: 'image', value: 'value'},
						{attribute_code: 'notimage', value: 'value'},
					],
				},
				{
					id: 2,
					sku: 'sku2',
					name: 'prod2',
					price: 5,
					custom_attributes: [
						{attribute_code: 'image', value: 'value'},
						{attribute_code: 'notimage', value: 'value'},
					],
				},
			]},
		],
		subState: false,
	},
};

const store = mockStore(props);
rendered = renderer.create(
	<Provider store={store}>
		<Router><Catalog match={{params: {id: 1}}} /></Router></Provider>
);
instance = rendered.root;
describe('Catalog Component', () => {
	it('Check the props, if returned JSX is correctly', () => {
		instance.findAllByType('button')[0].props.onClick();
		rendered.unmount();
	});

	it('All possibles situations', () => {
		props.catalog.category.level = 3;
		props.catalog.categories[0].childrenItems=props.catalog.categories[0].items;
		props.catalog.categories[0].items = [];
		props.catalog.categories[0].children = '1,2,3';
		const store = mockStore(props);
		rendered = renderer.create(
			<Provider store={store}>
				<Router><Catalog match={{params: {id: 1}}} /></Router></Provider>
		);
	});

	it('All possibles situations', () => {
		props.catalog.category = false;
		props.catalog.categories = false;
		const store = mockStore(props);
		rendered = renderer.create(
			<Provider store={store}>
				<Router><Catalog match={{params: {id: 1}}} /></Router></Provider>
		);
		instance = rendered.root;
		rendered.update([]);
		rendered.unmount();
	});
});
