import React from 'react';
import Register from '../../../components/Auth/Register';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

const mockStore = configureStore();
let rendered;
let instance = '';

const props = {
	common: {
		token: false,
	},
	auth: {
		fname: '',
		lname: '',
		emailR: '',
		passwordR: '',
		cpasswordR: '',
		history: {push: () => {}},
		errorsRegister: [
			{key: 'Error', error: 'Error1'},
			{key: 'Error', error: 'Error2'},
			{key: 'Error', error: 'Error3'},
		],
		passLogType: 'password',
	},
};
const store = mockStore(props);
rendered = renderer.create(
	<Provider store={store}><Router><Register /></Router></Provider>
);
instance = rendered.root;
describe('Register Component', () => {
	it('Check the props, if returned JSX is correctly and call all the functions',
		() => {
			expect(instance.props.store.getState()).toEqual(props);
			instance.findByType('form').props.onSubmit({preventDefault: () => {}});
			instance.findAllByType('input')[0].props.onChange({target: {value: 'd'}});
		}
	);

	it('All possibles situations', () => {
		props.auth.fname = 'Dani';
		props.auth.lname = 'Ortiz';
		props.auth.emailR = 'dani@gmail.com';
		props.auth.passwordR = 'Password.1';
		props.auth.cpasswordR = 'Password.1';
		props.auth.errorsRegister = undefined;
		const store = mockStore(props);
		rendered = renderer.create(
			<Provider store={store}><Router><Register /></Router></Provider>
		);
		instance = rendered.root;
		instance.findByType('form').props.onSubmit({preventDefault: () => {}});
		instance.findAllByType('i')[0].props.onClick();
		rendered.unmount();
	});

	it('All possibles situations', () => {
		props.common.token = 'token';
		const store = mockStore(props);
		rendered = renderer.create(
			<Provider store={store}><Router><Register /></Router></Provider>
		);
		rendered.unmount();
	});
});
