import {validate} from '../../../components/Auth/validate';

describe('Validate Functions', () => {
	it('ValidateFormLog', async () => {
		expect(await validate.validateFormLog('', '')).toEqual([
			{key: 'Email', error: 'Email is required'},
			{key: 'Password', error: 'Password is required'},
		]);
		expect(await validate.validateFormLog('dani', 'pass')).toEqual([
			{key: 'Email', error: 'Email is invalid'},
			{key: 'Password',
				error: 'Password minum length is 8, 1 Lowercase, 1 Uppercase,' +
			'1 Numeric Character, 1 Especial Character'},
		]);
		expect(await validate.validateFormLog('dani@gmail.com', 'Password1.'))
			.toEqual([]);
	});

	it('ValidateFormReg', async () => {
		expect(await validate.validateFormReg('', '', '', '', '')).toEqual([
			{key: 'First Name', error: 'First Name is required'},
			{key: 'Last Name', error: 'Last Name is required'},
			{key: 'Email', error: 'Email is required'},
			{key: 'Password', error: 'Password is required'},
		]);
		expect(await validate.validateFormReg('Da ', 'Or ', 'dani', 'pass', ''))
			.toEqual([
				{key: 'First Name', error: 'First Name can\'t contains spaces'},
				{key: 'Last Name', error: 'Last Name can\'t contains spaces'},
				{key: 'Email', error: 'Email is invalid'},
				{key: 'Password',
					error: 'Password minum length is 8, 1 Lowercase, 1 Uppercase,' +
				'1 Numeric Character, 1 Especial Character'},
			]);
		expect(await validate.validateFormReg('Da', 'Or', 'dani', 'Password1.', ''))
			.toEqual([
				{key: 'First Name', error: 'First Name must be between 3 & 15 letters'},
				{key: 'Last Name', error: 'Last Name must be between 3 & 15 letters'},
				{key: 'Email', error: 'Email is invalid'},
				{key: 'Password',
					error: 'Password d\'ont match'},
			]);
		expect(await validate.validateFormReg(
			'Dani', 'Ortiz', 'dani@gmail.com', 'Password1.', 'Password1.'))
			.toEqual([]);
	});
});
