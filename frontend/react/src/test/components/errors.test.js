import React from 'react';
import Errors from '../../components/Errors';
import renderer from 'react-test-renderer';

let rendered = '';
let instance = '';

const props = {
	errors: [
		{key: 'Error', error: 'Error1'},
		{key: 'Error', error: 'Error2'},
		{key: 'Error', error: 'Error3'},
	],
};

rendered = renderer.create(<Errors {...props}/>);
instance = rendered.root;

describe('Errors Component', () => {
	it('Check the props, if returned JSX is correctly', () => {
		expect(instance.props).toEqual(props);
		expect(instance.findAllByType('p').length).toEqual(3);
		expect(instance.findAllByProps({
			className: 'm-3 alert alert-danger small',
		})[0].props.children).toContain('Error1');
	});

	it('All possibles situations', () => {
		props.errors = undefined;
		rendered = renderer.create(<Errors {...props}/>);
		instance = rendered.root;
		expect(instance.findByType('section').children).toEqual(['']);
	});
});
