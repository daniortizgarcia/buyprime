import {utils} from '../utils';

const param = {};
describe('Utils Functions', () => {
	it('Utils', () => {
		expect(utils.checkIfExist(param, 'value')).toEqual('value');
		expect(utils.checkIfExist(param, 'value', 'param'))
			.toEqual({param: 'value'});
		expect(utils.checkIfExist(false, 'value')).toEqual(false);
	});
});
