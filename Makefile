.PHONY: up
up:
	kubectl apply -f deployment.yml
	kubectl apply -f lb.yml

.PHONY: down
down:
	kubectl delete deployment buyprime-deployment
	kubectl delete services buyprime-lb

.PHONY: build
build:
	cd frontend && docker build -t devo/buyprime_frontend:v1 -f Dockerfile . && cd ..
	cd backend && docker build -t devo/buyprime_backend:v1 -f Dockerfile . && cd ..

.PHONY: remove
remove:
	docker image rm -f devo/buyprime_frontend:v1
	docker image rm -f devo/buyprime_backend:v1